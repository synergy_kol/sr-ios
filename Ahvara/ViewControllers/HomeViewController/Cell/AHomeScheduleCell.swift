//
//  AHomeScheduleCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 01/01/22.
//

import UIKit

class AHomeScheduleCell: UITableViewCell {

    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
