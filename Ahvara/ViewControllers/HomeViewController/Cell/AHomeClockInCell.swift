//
//  AHomeClockInCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 01/01/22.
//

import UIKit

class AHomeClockInCell: UITableViewCell {

    @IBOutlet weak var btnStartShift: UIButton!
    @IBOutlet weak var btnMarkArrived: UIButton!
    @IBOutlet weak var btnDirection: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
