//
//  AHomeCalendarCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 01/01/22.
//

import UIKit

class AHomeCalendarCell: UITableViewCell {

    var completionBlockDidSelectTableViewCellAction: DidSelectTableViewCellCompletionBlock?
    @IBOutlet weak var tableviewHomeScheduleList: UITableView!
    @IBOutlet weak var btnReschedule: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    //@IBOutlet weak var objCalendarView: HorizontalCalendar!
    
    var selectedIndex = Set<Int>()
    var selectedType : String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
      
        // Configure the view for the selected state
    }
    
    func setupCellData(objModel:AHomeViewModel, index : Int) {
        self.tableviewHomeScheduleList.reloadData()
        
        /*let calendar = HorizontalCalendar.initialize()
         objCalendarView = calendar
         objCalendarView.backgroundColor = .white
         objCalendarView.onSelectionChanged = { date in
             print(date)
         }*/
    }

}

extension AHomeCalendarCell : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AHomeScheduleCell", for: indexPath) as! AHomeScheduleCell;
        
        if(indexPath.row == 0) {
            cell.btnStatus.isHidden = false
            cell.lblDate.isHidden = true
            cell.btnStatus.backgroundColor = #colorLiteral(red: 0.5333333333, green: 0.7882352941, blue: 0.1921568627, alpha: 1)
            cell.btnStatus.setTitle("Completed", for: .normal)
            selectedType = "completed"
        } else if(indexPath.row == 2) {
            cell.btnStatus.isHidden = false
            cell.btnStatus.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.3019607843, blue: 0.3333333333, alpha: 1)
            cell.lblDate.isHidden = false
            cell.lblDate.textColor =  #colorLiteral(red: 0.2549019608, green: 0.3019607843, blue: 0.3333333333, alpha: 1)
            cell.btnStatus.setTitle("Rescheduled", for: .normal)
            selectedType = "rescheduled"
        } else if(indexPath.row == 3) {
            cell.btnStatus.isHidden = false
            cell.lblDate.isHidden = false
            cell.lblDate.textColor = #colorLiteral(red: 0.8745098039, green: 0, blue: 0, alpha: 1)
            cell.btnStatus.backgroundColor = #colorLiteral(red: 0.8745098039, green: 0, blue: 0, alpha: 1)
            cell.btnStatus.setTitle("Cancelled", for: .normal)
            selectedType = "cancelled"
        } else if(indexPath.row == 5) {
            cell.btnStatus.isHidden = false
            cell.lblDate.isHidden = true
            cell.btnStatus.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.6039215686, blue: 0.7882352941, alpha: 1)
            cell.btnStatus.setTitle("Arrived", for: .normal)
            selectedType = "arrived"
        } else {
            cell.btnStatus.isHidden = true
            cell.lblDate.isHidden = true
            selectedType = "scheduled"
        }
        
        cell.btnSelect.removeControlEvent(.touchUpInside)
        cell.btnSelect.addControlEvent(.touchUpInside){
            cell.btnSelect.isSelected = !cell.btnSelect.isSelected
            
            if (self.selectedIndex.contains(indexPath.row)) {
                self.selectedIndex.remove(indexPath.row)
            } else {
                self.selectedIndex.insert(indexPath.row)
            }
        }
        
        if(self.selectedIndex.contains(indexPath.row)) {
            cell.btnSelect.isSelected = true
        } else {
            cell.btnSelect.isSelected = false
        }
        
        return cell;
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
    
    if(indexPath.row == 0) {
        selectedType = "completed"
    } else if(indexPath.row == 2) {
        selectedType = "reschedule"
    } else if(indexPath.row == 3) {
        selectedType = "cancel"
    } else if(indexPath.row == 5) {
        selectedType = "arrived"
    } else {
        selectedType = "scheduled"
    }
    self.completionBlockDidSelectTableViewCellAction!(indexPath.row, ["selectedType" :selectedType ?? ""])
  }
}

