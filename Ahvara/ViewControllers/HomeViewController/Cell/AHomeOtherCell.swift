//
//  AHomeOtherCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 01/01/22.
//

import UIKit

class AHomeOtherCell: UITableViewCell {

    @IBOutlet weak var tableviewHomeTaskList: UITableView!
    @IBOutlet weak var imageViewHeader: UIImageView!
    @IBOutlet weak var lblHeader: UILabel!
    var selectedIndex : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellData(objModel:AHomeViewModel, index : Int) {
        if(index == 3) {
            self.lblHeader.text = "Tasks"
            self.imageViewHeader.image = #imageLiteral(resourceName: "IconTasks")
        } else {
            self.lblHeader.text = "Leads"
            self.imageViewHeader.image = #imageLiteral(resourceName: "IconGroup")
        }
        self.tableviewHomeTaskList.reloadData()
    }

}

extension AHomeOtherCell : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AHomeTaskCell", for: indexPath) as! AHomeTaskCell;
        //cell.setupCellData(objModel: self.objHomeViewModel)
        return cell;
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
  }
    
}


