//
//  AHomeViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 01/01/22.
//

import UIKit

class AHomeViewController: BaseViewController {

    @IBOutlet var objHomeViewModel: AHomeViewModel!
    @IBOutlet weak var tableviewHomeList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showHomeBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.showButtonRight1()
        self.showButtonRight2()
        self.hideButtonRight3()
        //let userName = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.username, defaultValue: "")
        self.setTitleLabelText(text: "Welcome, Nicholas", titleColor: #colorLiteral(red: 0.1529411765, green: 0.3411764706, blue: 0.2980392157, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconMenu"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    private func initializeView(){
        tableviewHomeList.rowHeight = UITableView.automaticDimension
        tableviewHomeList.estimatedRowHeight = 40.0
        tableviewHomeList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewHomeList.frame.size.width, height: 100))
        self.tableviewHomeList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData(){
        
        self.tableviewHomeList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
}

extension AHomeViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AHomeClockInCell", for: indexPath) as! AHomeClockInCell;
            
            cell.btnStartShift.removeControlEvent(.touchUpInside)
            cell.btnStartShift.addControlEvent(.touchUpInside){
                if(cell.btnStartShift.isSelected) {
                    let vc:AClockOutPopupViewController =  Common.getVCWithId(identifier: "AClockOutPopupViewController", storyBoardName: "Home") as! AClockOutPopupViewController
                                   
                        vc.callbackFromClockOutPopup = { selectedDataValue in
                            
                            let isProceed = selectedDataValue["isProceed"] as? Bool
                            if(isProceed ?? false) {
                                cell.btnStartShift.isSelected = !(cell.btnStartShift.isSelected)
                            }
                    }
                    
                    vc.modalPresentationStyle = .overCurrentContext
                    self.present(vc, animated: true, completion: nil)
                } else {
                    let vc:AClockInPopupViewController =  Common.getVCWithId(identifier: "AClockInPopupViewController", storyBoardName: "Home") as! AClockInPopupViewController
                                   
                        vc.callbackFromClockInPopup = { selectedDataValue in
                            
                            let isProceed = selectedDataValue["isProceed"] as? Bool
                            if(isProceed ?? false) {
                                cell.btnStartShift.isSelected = !(cell.btnStartShift.isSelected)
                            }
                    }
                    
                    vc.modalPresentationStyle = .overCurrentContext
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
            cell.btnMarkArrived.removeControlEvent(.touchUpInside)
            cell.btnMarkArrived.addControlEvent(.touchUpInside){
                cell.btnMarkArrived.isSelected = !cell.btnDirection.isSelected
            }
            
            cell.btnDirection.removeControlEvent(.touchUpInside)
            cell.btnDirection.addControlEvent(.touchUpInside){
                let vc:ADirectionViewController =  Common.getVCWithId(identifier: "ADirectionViewController", storyBoardName: "Home") as! ADirectionViewController
                    self.navigationController?.pushViewController(vc, animated: true)
            }
            
            return cell;
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AHomeProgressCell", for: indexPath) as! AHomeProgressCell;
            return cell;
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AHomeCalendarCell", for: indexPath) as! AHomeCalendarCell;
            
            cell.setupCellData(objModel: self.objHomeViewModel, index: indexPath.row )
            
            cell.btnReschedule.removeControlEvent(.touchUpInside)
            cell.btnReschedule.addControlEvent(.touchUpInside){
                let vc:ARescheduleViewController =  Common.getVCWithId(identifier: "ARescheduleViewController", storyBoardName: "Home") as! ARescheduleViewController
                    self.navigationController?.pushViewController(vc, animated: true)
            }
            
            cell.btnCancel.removeControlEvent(.touchUpInside)
            cell.btnCancel.addControlEvent(.touchUpInside){
                let vc:ACancelViewController =  Common.getVCWithId(identifier: "ACancelViewController", storyBoardName: "Home") as! ACancelViewController
                    self.navigationController?.pushViewController(vc, animated: true)
            }
            
            cell.completionBlockDidSelectTableViewCellAction = { (index, value ) in
                
                let data : Dictionary<String, AnyObject> = value as! Dictionary<String, AnyObject>
                
                let vc:AAppointmentDetailsViewController =  Common.getVCWithId(identifier: "AAppointmentDetailsViewController", storyBoardName: "Home") as! AAppointmentDetailsViewController
                vc.selectedType = data["selectedType"] as? String
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return cell
        case 3,4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AHomeOtherCell", for: indexPath) as! AHomeOtherCell;
            cell.setupCellData(objModel: self.objHomeViewModel, index: indexPath.row)
            return cell
        default:
            return UITableViewCell.init()
        }
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
  }
    
}


