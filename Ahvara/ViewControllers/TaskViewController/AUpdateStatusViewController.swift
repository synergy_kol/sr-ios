//
//  AUpdateStatusViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 05/01/22.
//

import UIKit

class AUpdateStatusViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
        
    
    @IBAction func actionCancel(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionProceed(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }

}
