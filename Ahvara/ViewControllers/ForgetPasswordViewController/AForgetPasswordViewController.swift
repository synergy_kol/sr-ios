//
//  AForgetPasswordViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 24/12/21.
//

import UIKit

class AForgetPasswordViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        
        let vc:AResetPasswordViewController =  Common.getVCWithId(identifier: "AResetPasswordViewController", storyBoardName: "Main") as! AResetPasswordViewController
            self.navigationController?.pushViewController(vc, animated: true)
    }
}
