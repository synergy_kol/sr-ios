//
//  AResetPasswordViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 24/12/21.
//

import UIKit

class AResetPasswordViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        
        let vc:APasswordUpdatedViewController =  Common.getVCWithId(identifier: "APasswordUpdatedViewController", storyBoardName: "Main") as! APasswordUpdatedViewController
            self.navigationController?.pushViewController(vc, animated: true)
    }

}
