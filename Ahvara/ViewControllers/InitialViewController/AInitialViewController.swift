//
//  AInitialViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 23/12/21.
//

import UIKit

class AInitialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionSignIn(_ sender: UIButton) {
        let vc:ALoginViewController =  Common.getVCWithId(identifier: "ALoginViewController", storyBoardName: "Main") as! ALoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
