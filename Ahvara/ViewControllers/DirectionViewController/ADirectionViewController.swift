//
//  ADirectionViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 02/01/22.
//

import UIKit

class ADirectionViewController: BaseViewController {
    
    @IBOutlet weak var collectioviewAppointmentInfoList: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showInitialBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.hideButtonRight3()
        self.setTitleLabelText(text: "Route Planner Map View", titleColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconArrowBack1"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func actionButtonLeft1() {
        self.navigationController?.popViewController(animated:true)
    }
    
    private func initializeView(){
        self.loadTableData()
    }
    
    
    // To get the Table data
    private func loadTableData(){
        
        self.collectioviewAppointmentInfoList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
}

extension ADirectionViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AAppointmentTaskInfoCell", for: indexPath) as! AAppointmentTaskInfoCell
        return cell;
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let width : CGFloat = ((UIScreen.main.bounds.width) / 4)
        return CGSize(width: 380, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
    }
}

