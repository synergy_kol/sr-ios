//
//  ALoginViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 24/12/21.
//

import UIKit

class ALoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionForgetPassword(_ sender: UIButton) {
        let vc:AForgetPasswordViewController =  Common.getVCWithId(identifier: "AForgetPasswordViewController", storyBoardName: "Main") as! AForgetPasswordViewController
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        
        let vc:ALoginSuccessViewController =  Common.getVCWithId(identifier: "ALoginSuccessViewController", storyBoardName: "Main") as! ALoginSuccessViewController
            self.navigationController?.pushViewController(vc, animated: true)
    }

}
