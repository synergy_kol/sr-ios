//
//  ALoginSuccessViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 24/12/21.
//

import UIKit

class ALoginSuccessViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionSkip(_ sender: UIButton) {
        
        Common.changeRootController(rootControllerType: .RootControllerTypeHome)
    }
    
    @IBAction func actionStartYourDayNow(_ sender: UIButton) {
        
        Common.changeRootController(rootControllerType: .RootControllerTypeHome)
    }

}
