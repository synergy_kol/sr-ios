//
//  AReportViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 21/01/22.
//

import UIKit

class AReportViewController: BaseViewController {
    
    @IBOutlet weak var tableviewReportList: UITableView!
    @IBOutlet weak var txtfieldSearch: UITextField!

    
    var selectedTab : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showHomeBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.showButtonRight1()
        self.showButtonRight2()
        self.hideButtonRight3()
        //let userName = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.username, defaultValue: "")
        self.setTitleLabelText(text: "Reports", titleColor: #colorLiteral(red: 0.1529411765, green: 0.3411764706, blue: 0.2980392157, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconMenu"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    private func initializeView() {
        tableviewReportList.rowHeight = UITableView.automaticDimension
        tableviewReportList.estimatedRowHeight = 40.0
        tableviewReportList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewReportList.frame.size.width, height: 100))
        self.tableviewReportList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData() {
    
        self.tableviewReportList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
    @IBAction func actionApply(_ sender: UIButton) {
    }
}

extension AReportViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0,1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ADateCell", for: indexPath) as! ADateCell;
            
            return cell;
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ADropdownCell", for: indexPath) as! ADropdownCell;
            
            return cell;
        default:
            return UITableViewCell.init()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
    }
    
}
