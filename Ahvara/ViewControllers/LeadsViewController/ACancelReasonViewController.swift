//
//  ACancelReasonViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 21/01/22.
//

import UIKit

class ACancelReasonViewController: UIViewController {

    var callbackFromCancelReasonPopup : (([String : Any?]) -> Void)?
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnProceed: UIButton!
    
    var value = [String:Any?]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        value["isProceed"] = false
        callbackFromCancelReasonPopup?(value)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionProceed(_ sender: Any) {
        value["isProceed"] = true
        callbackFromCancelReasonPopup?(value)
        self.dismiss(animated: true, completion: nil)
    }

}
