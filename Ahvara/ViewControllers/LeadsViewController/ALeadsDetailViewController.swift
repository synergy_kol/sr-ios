//
//  ALeadsDetailViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 21/01/22.
//

import UIKit

class ALeadsDetailViewController:  BaseViewController {
    
    @IBOutlet weak var tableviewLeadsDetailsList: UITableView!
    @IBOutlet var btnUpdateStatus: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showInitialBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.hideButtonRight3()
        self.setTitleLabelText(text: "Leads Details", titleColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconBack"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func actionButtonLeft1() {
        self.navigationController?.popViewController(animated:true)
    }
    
    
    private func initializeView(){
        tableviewLeadsDetailsList.rowHeight = UITableView.automaticDimension
        tableviewLeadsDetailsList.estimatedRowHeight = 40.0
        tableviewLeadsDetailsList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewLeadsDetailsList.frame.size.width, height: 100))
        self.tableviewLeadsDetailsList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData() {
        
        self.tableviewLeadsDetailsList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
    @IBAction func actionConvert(_ sender: UIButton) {
    }
    @IBAction func actionCancelOrLost(_ sender: UIButton) {
        let vc:ACancelReasonViewController =  Common.getVCWithId(identifier: "ACancelReasonViewController", storyBoardName: "Home") as! ACancelReasonViewController
                       
            vc.callbackFromCancelReasonPopup = { selectedDataValue in
                
                let isProceed = selectedDataValue["isProceed"] as? Bool
                if(isProceed ?? false) {
                   // cell.btnStartShift.isSelected = !(cell.btnStartShift.isSelected)
                }
        }
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
}

extension ALeadsDetailViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 11
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0,1,2,3,4,5,6,7,10:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ATextInfoCell", for: indexPath) as! ATextInfoCell;
            cell.setupCellData(index: indexPath.row, isfromWorkOrder: false)
            return cell;
        case 8,9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ADateInfoCell", for: indexPath) as! ADateInfoCell;
            
            cell.setupCellData(index: indexPath.row)
            
            return cell;
        default:
            return UITableViewCell.init()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
        
    }
}
