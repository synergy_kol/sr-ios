//
//  ALeadsViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 18/01/22.
//

import UIKit

class ALeadsViewController: BaseViewController {
    
    @IBOutlet var objHomeViewModel: AHomeViewModel!
    @IBOutlet weak var tableviewLeadsList: UITableView!
    @IBOutlet weak var txtfieldSearch: UITextField!

    
    var selectedTab : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showHomeBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.showButtonRight1()
        self.showButtonRight2()
        self.hideButtonRight3()
        //let userName = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.username, defaultValue: "")
        self.setTitleLabelText(text: "Leads", titleColor: #colorLiteral(red: 0.1529411765, green: 0.3411764706, blue: 0.2980392157, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconMenu"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    private func initializeView(){
        tableviewLeadsList.rowHeight = UITableView.automaticDimension
        tableviewLeadsList.estimatedRowHeight = 40.0
        tableviewLeadsList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewLeadsList.frame.size.width, height: 100))
        self.tableviewLeadsList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData(){
    
        self.tableviewLeadsList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
    @IBAction func actionSelectTab(_ sender: UIButton) {
    }
}

extension ALeadsViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ALeadsDetailCell", for: indexPath) as! ALeadsDetailCell;
        
        return cell;
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
      let vc:ALeadsDetailViewController =  Common.getVCWithId(identifier: "ALeadsDetailViewController", storyBoardName: "Home") as! ALeadsDetailViewController
          
      self.navigationController?.pushViewController(vc, animated: true)
  }
}

