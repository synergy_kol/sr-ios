//
//  ADateInfoCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 21/01/22.
//

import UIKit

class ADateInfoCell: UITableViewCell {

    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellData(index : Int) {
        if(index == 8) {
            self.lblCategoryName.text = "Est. Time To Close"
            self.lblValue.text = "November 06, 2021"
        } else if(index == 9) {
            self.lblCategoryName.text = "Reminder Date And Time"
            self.lblValue.text = "November 05, 2021"
        }
    }

}
