//
//  ATextInfoCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 21/01/22.
//

import UIKit

class ATextInfoCell: UITableViewCell {

    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellData(index : Int , isfromWorkOrder: Bool) {
        
        if(isfromWorkOrder) {
            if(index == 2) {
                self.lblCategoryName.text = "Amount"
                self.lblValue.text = "$6500"
            } else if(index == 3) {
                self.lblCategoryName.text = "Type"
                self.lblValue.text = "Unit Sales"
            } else if(index == 4) {
                self.lblCategoryName.text = "Unit"
                self.lblValue.text = "Lorem Ipsum 2235"
            }else if(index == 5) {
                self.lblCategoryName.text = "Parts"
                self.lblValue.text = "Lorem Ipsum 2235"
            }else if(index == 6) {
                self.lblCategoryName.text = "Details"
                self.lblValue.text = "Lorem ipsum dolor sit amet, consetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            }
        } else {
            if(index == 0) {
                self.lblCategoryName.text = "Customer"
                self.lblValue.text = "Steve Smith"
            } else if(index == 1) {
                self.lblCategoryName.text = "Phone"
                self.lblValue.text = "123 456 7890"
            } else if(index == 2) {
                self.lblCategoryName.text = "Email"
                self.lblValue.text = "steve.smith007@gmail.com"
            } else if(index == 3) {
                self.lblCategoryName.text = "Address 1"
                self.lblValue.text = "47 West 13th Street"
            } else if(index == 4) {
                self.lblCategoryName.text = "City"
                self.lblValue.text = "New York"
            } else if(index == 5) {
                self.lblCategoryName.text = "State"
                self.lblValue.text = "New York"
            } else if(index == 6) {
                self.lblCategoryName.text = "Zip Code"
                self.lblValue.text = "10011"
            }else if(index == 7) {
                self.lblCategoryName.text = "Machine/Unit"
                self.lblValue.text = "Machine Name"
            }else if(index == 10) {
                self.lblCategoryName.text = "Notes"
                self.lblValue.text = "Lorem ipsum dolor sit amet, consetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            }
        }
    }
}
