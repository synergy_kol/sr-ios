//
//  AUnitDetailsViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 07/01/22.
//

import UIKit

class AUnitDetailsViewController: BaseViewController {
    
    @IBOutlet weak var tableviewUnitDetailsList: UITableView!
    @IBOutlet var btnUpdateStatus: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showInitialBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.hideButtonRight3()
        self.setTitleLabelText(text: "Task Title", titleColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconBack"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func actionButtonLeft1() {
        self.navigationController?.popViewController(animated:true)
    }
    
    
    private func initializeView(){
        tableviewUnitDetailsList.rowHeight = UITableView.automaticDimension
        tableviewUnitDetailsList.estimatedRowHeight = 40.0
        tableviewUnitDetailsList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewUnitDetailsList.frame.size.width, height: 100))
        self.tableviewUnitDetailsList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData(){
        
        self.tableviewUnitDetailsList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }

    @IBAction func actionSale(_ sender: UIButton) {
    }
    @IBAction func actionRental(_ sender: UIButton) {
    }
    
}

extension AUnitDetailsViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AUnitDescriptionCell", for: indexPath) as! AUnitDescriptionCell;
            
            cell.completionBlockDidSelectCollectionViewCellAction = { (index, value ) in
            }
            return cell;
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AForwardCell", for: indexPath) as! AForwardCell;
            
            return cell;
        default:
            return UITableViewCell.init()
        }
        
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
  }
}


