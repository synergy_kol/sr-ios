//
//  AForwardCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 07/01/22.
//

import UIKit

class AForwardCell: UITableViewCell {

    @IBOutlet weak var lblCategoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellData(index : Int) {
        if(index == 1) {
            self.lblCategoryName.text = "View Contacts"
        } else if(index == 4) {
            self.lblCategoryName.text = "View Customer History"
        }else if(index == 5) {
            self.lblCategoryName.text = "View Documents"
        }else if(index == 6) {
            self.lblCategoryName.text = "View Unit Details"
        }
    }

}
