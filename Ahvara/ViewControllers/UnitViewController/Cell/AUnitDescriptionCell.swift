//
//  AUnitDescriptionCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 07/01/22.
//

import UIKit

class AUnitDescriptionCell: UITableViewCell {

    var completionBlockDidSelectCollectionViewCellAction: DidSelectCollectionCellCompletionBlock?
    @IBOutlet weak var collectioviewTagList: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCellData() {
        self.collectioviewTagList.reloadData()
        
        /*let calendar = HorizontalCalendar.initialize()
         objCalendarView = calendar
         objCalendarView.backgroundColor = .white
         objCalendarView.onSelectionChanged = { date in
             print(date)
         }*/
    }
}

extension AUnitDescriptionCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AUnitTagCell", for: indexPath) as! AUnitTagCell
        return cell;
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let width : CGFloat = ((UIScreen.main.bounds.width - 95) / 4)
        return CGSize(width: width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        self.completionBlockDidSelectCollectionViewCellAction!(indexPath.row, ["selectedType" :""])
    }
}


