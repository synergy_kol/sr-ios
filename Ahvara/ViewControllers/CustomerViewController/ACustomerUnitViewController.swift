//
//  ACustomerUnitViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 19/01/22.
//

import UIKit

class ACustomerUnitViewController: BaseViewController {
    
    @IBOutlet weak var tableviewCustomerUnitList: UITableView!
    @IBOutlet weak var txtfieldSearch: UITextField!

    
    var selectedTab : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showInitialBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.hideButtonRight3()
        self.setTitleLabelText(text: "Customer Unit", titleColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconBack"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func actionButtonLeft1() {
        self.navigationController?.popViewController(animated:true)
    }
    
    private func initializeView(){
        tableviewCustomerUnitList.rowHeight = UITableView.automaticDimension
        tableviewCustomerUnitList.estimatedRowHeight = 40.0
        tableviewCustomerUnitList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewCustomerUnitList.frame.size.width, height: 100))
        self.tableviewCustomerUnitList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData(){
    
        self.tableviewCustomerUnitList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
    @IBAction func actionSort(_ sender: UIButton) {
    }
    @IBAction func actionFilter(_ sender: UIButton) {
    }
}

extension ACustomerUnitViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ACustomerUnitDetailCell", for: indexPath) as! ACustomerUnitDetailCell;
        
        return cell;
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
      let vc:ACustomerUnitDetailsViewController =  Common.getVCWithId(identifier: "ACustomerUnitDetailsViewController", storyBoardName: "Home") as! ACustomerUnitDetailsViewController
          
      self.navigationController?.pushViewController(vc, animated: true)
  }
}
