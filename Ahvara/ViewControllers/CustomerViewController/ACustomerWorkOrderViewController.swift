//
//  ACustomerWorkOrderViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 21/01/22.
//

import UIKit

class ACustomerWorkOrderViewController: BaseViewController {
    
    @IBOutlet weak var tableviewCustomerWorkOrderList: UITableView!
    @IBOutlet var btnUpdateStatus: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showInitialBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.hideButtonRight3()
        self.setTitleLabelText(text: "Customer Work Order", titleColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconBack"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func actionButtonLeft1() {
        self.navigationController?.popViewController(animated:true)
    }
    
    
    private func initializeView(){
        tableviewCustomerWorkOrderList.rowHeight = UITableView.automaticDimension
        tableviewCustomerWorkOrderList.estimatedRowHeight = 40.0
        tableviewCustomerWorkOrderList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewCustomerWorkOrderList.frame.size.width, height: 100))
        self.tableviewCustomerWorkOrderList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData(){
        
        self.tableviewCustomerWorkOrderList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
    @IBAction func actionSort(_ sender: UIButton) {
    }
    @IBAction func actionFilter(_ sender: UIButton) {
    }
}

extension ACustomerWorkOrderViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AWorkorderCell", for: indexPath) as! AWorkorderCell;
        
        return cell;
        
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
  }
}
