//
//  ACustomerListViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 18/01/22.
//

import UIKit

class ACustomerListViewController: BaseViewController {
    
    @IBOutlet weak var tableviewCustomerList: UITableView!
    @IBOutlet weak var txtfieldSearch: UITextField!

    
    var selectedTab : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showHomeBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.showButtonRight1()
        self.showButtonRight2()
        self.hideButtonRight3()
        //let userName = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.username, defaultValue: "")
        self.setTitleLabelText(text: "Customer", titleColor: #colorLiteral(red: 0.1529411765, green: 0.3411764706, blue: 0.2980392157, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconMenu"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    private func initializeView(){
        tableviewCustomerList.rowHeight = UITableView.automaticDimension
        tableviewCustomerList.estimatedRowHeight = 40.0
        tableviewCustomerList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewCustomerList.frame.size.width, height: 100))
        self.tableviewCustomerList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData(){
    
        self.tableviewCustomerList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
    @IBAction func actionAddCustomer(_ sender: UIButton) {
        let vc:AAddCustomerViewController =  Common.getVCWithId(identifier: "AAddCustomerViewController", storyBoardName: "Home") as! AAddCustomerViewController
            
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionSort(_ sender: UIButton) {
    }
    @IBAction func actionFilter(_ sender: UIButton) {
    }
}

extension ACustomerListViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ACustomerInfoCell", for: indexPath) as! ACustomerInfoCell;
        
        return cell;
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
      
      let vc:ACustomerDetailsViewController =  Common.getVCWithId(identifier: "ACustomerDetailsViewController", storyBoardName: "Home") as! ACustomerDetailsViewController
          
      self.navigationController?.pushViewController(vc, animated: true)
  }
}
