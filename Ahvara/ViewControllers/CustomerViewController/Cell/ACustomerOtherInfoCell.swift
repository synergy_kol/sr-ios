//
//  ACustomerOtherInfoCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 18/01/22.
//

import UIKit

class ACustomerOtherInfoCell: UITableViewCell {

    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var imgviewArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCellData(index : Int) {
        if(index == 7) {
            self.lblCategoryName.text = "View Work Order:"
            self.lblCount.text = "0"
            self.imgviewArrow.isHidden = false
        } else if(index == 8) {
            self.lblCategoryName.text = "Lead/Deal Status:"
            self.lblCount.text = "0"
            self.imgviewArrow.isHidden = true
        }
    }
}
