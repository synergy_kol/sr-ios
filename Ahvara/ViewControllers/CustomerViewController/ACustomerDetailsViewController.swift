//
//  ACustomerDetailsViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 18/01/22.
//

import UIKit

class ACustomerDetailsViewController: BaseViewController {
    
    @IBOutlet weak var tableviewCustomerDetailsList: UITableView!
    @IBOutlet var btnUpdateStatus: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showInitialBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.hideButtonRight3()
        self.setTitleLabelText(text: "Customer Details", titleColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconBack"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func actionButtonLeft1() {
        self.navigationController?.popViewController(animated:true)
    }
    
    
    private func initializeView(){
        tableviewCustomerDetailsList.rowHeight = UITableView.automaticDimension
        tableviewCustomerDetailsList.estimatedRowHeight = 40.0
        tableviewCustomerDetailsList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewCustomerDetailsList.frame.size.width, height: 100))
        self.tableviewCustomerDetailsList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData(){
        
        self.tableviewCustomerDetailsList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
}

extension ACustomerDetailsViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ACustomerDetailCell", for: indexPath) as! ACustomerDetailCell;
            
            return cell;
        case 1,4,5,6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AForwardCell", for: indexPath) as! AForwardCell;
            
            cell.setupCellData(index: indexPath.row)
            
            return cell;
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ACustomerCategoryListCell", for: indexPath) as! ACustomerCategoryListCell;
            
            return cell;
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ACustomerSiteDetailsCell", for: indexPath) as! ACustomerSiteDetailsCell;
            
            return cell;
        case 7,8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ACustomerOtherInfoCell", for: indexPath) as! ACustomerOtherInfoCell;
            
            cell.setupCellData(index: indexPath.row)
            
            return cell;
        default:
            return UITableViewCell.init()
        }
        
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
      if(indexPath.row == 4) {
          let vc:ACustomerHistoryViewController =  Common.getVCWithId(identifier: "ACustomerHistoryViewController", storyBoardName: "Home") as! ACustomerHistoryViewController
              
          self.navigationController?.pushViewController(vc, animated: true)
      }
      else if(indexPath.row == 5) {
          let vc:ACustomerDocumentViewController =  Common.getVCWithId(identifier: "ACustomerDocumentViewController", storyBoardName: "Home") as! ACustomerDocumentViewController
              
          self.navigationController?.pushViewController(vc, animated: true)
      }
      else if(indexPath.row == 6) {
          let vc:ACustomerUnitViewController =  Common.getVCWithId(identifier: "ACustomerUnitViewController", storyBoardName: "Home") as! ACustomerUnitViewController
              
          self.navigationController?.pushViewController(vc, animated: true)
      }
      else if(indexPath.row == 7) {
          let vc:ACustomerWorkOrderViewController =  Common.getVCWithId(identifier: "ACustomerWorkOrderViewController", storyBoardName: "Home") as! ACustomerWorkOrderViewController
              
          self.navigationController?.pushViewController(vc, animated: true)
      }
            
  }
}


