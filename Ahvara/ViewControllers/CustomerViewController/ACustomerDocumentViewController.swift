//
//  ACustomerDocumentViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 19/01/22.
//

import UIKit

class ACustomerDocumentViewController:  BaseViewController {
    
    @IBOutlet weak var tableviewCustomerDocumentList: UITableView!
    @IBOutlet weak var txtfieldSearch: UITextField!

    
    var selectedTab : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showInitialBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.hideButtonRight3()
        self.setTitleLabelText(text: "Customer Documents", titleColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconBack"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func actionButtonLeft1() {
        self.navigationController?.popViewController(animated:true)
    }
    
    private func initializeView() {
        tableviewCustomerDocumentList.rowHeight = UITableView.automaticDimension
        tableviewCustomerDocumentList.estimatedRowHeight = 40.0
        tableviewCustomerDocumentList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewCustomerDocumentList.frame.size.width, height: 100))
        self.tableviewCustomerDocumentList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData() {
    
        self.tableviewCustomerDocumentList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
    @IBAction func actionSort(_ sender: UIButton) {
    }
    @IBAction func actionFilter(_ sender: UIButton) {
    }
}

extension ACustomerDocumentViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ACustomerDocumentInfoCell", for: indexPath) as! ACustomerDocumentInfoCell;
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
    }
    
}
