//
//  AScheduleViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 07/01/22.
//

import UIKit

class AScheduleViewController: BaseViewController {
    
    @IBOutlet var objHomeViewModel: AHomeViewModel!
    @IBOutlet weak var tableviewScheduleList: UITableView!
    @IBOutlet weak var viewSearchOrCalendar: UIView!
    @IBOutlet weak var constViewSearchOrCalendar: NSLayoutConstraint!
    @IBOutlet weak var txtfieldSearch: UITextField!
    @IBOutlet weak var btnUpcoming: UIButton!
    @IBOutlet weak var btnPast: UIButton!
    @IBOutlet weak var btnCalendar: UIButton!
    
    var selectedTab : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showHomeBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.showButtonRight1()
        self.showButtonRight2()
        self.hideButtonRight3()
        //let userName = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.username, defaultValue: "")
        self.setTitleLabelText(text: "Welcome, Nicholas", titleColor: #colorLiteral(red: 0.1529411765, green: 0.3411764706, blue: 0.2980392157, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconMenu"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    private func initializeView(){
        tableviewScheduleList.rowHeight = UITableView.automaticDimension
        tableviewScheduleList.estimatedRowHeight = 40.0
        tableviewScheduleList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewScheduleList.frame.size.width, height: 100))
        self.tableviewScheduleList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData(){
        
        self.constViewSearchOrCalendar.constant = 100.0
        self.btnUpcoming.isSelected = true
        self.btnPast.isSelected = false
        self.btnCalendar.isSelected = false
        self.tableviewScheduleList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
    @IBAction func actionSelectTab(_ sender: UIButton) {
        self.selectedTab = sender.tag
        sender.isSelected = !sender.isSelected
       
        switch sender.tag {
        case 1:
            self.btnPast.isSelected = false
            self.btnCalendar.isSelected = false
            self.txtfieldSearch.isHidden = false
            self.constViewSearchOrCalendar.constant = 100.0
            break
        case 2:
            self.btnUpcoming.isSelected = false
            self.btnCalendar.isSelected = false
            self.txtfieldSearch.isHidden = false
            self.constViewSearchOrCalendar.constant = 100.0
            break
        case 3:
            self.btnUpcoming.isSelected = false
            self.btnPast.isSelected = false
            self.txtfieldSearch.isHidden = true
            self.constViewSearchOrCalendar.constant = 230.0
            break
        default:
            break
        }
        
        self.tableviewScheduleList.reloadData()
    }
}

extension AScheduleViewController : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch selectedTab {
        case 1:
            return 2
        case 2:
            return 2
        case 3:
            return 1
        default:
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AScheduleHeaderCell", for: indexPath) as! AScheduleHeaderCell;
            
            if(indexPath.section == 0) {
                cell.lblDate.text = "September 8, 2021"
            } else {
                cell.lblDate.text = "September 9, 2021"
            }
            return cell;
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AScheduleDetailsCell", for: indexPath) as! AScheduleDetailsCell;
            
            return cell;
        }
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
  }
}
