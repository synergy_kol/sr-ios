//
//  AExpenseDetailsCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 02/01/22.
//

import UIKit

class AExpenseDetailsCell: UITableViewCell {

    @IBOutlet weak var lblExpenseTypeName: UILabel!
    @IBOutlet weak var lblCost: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
