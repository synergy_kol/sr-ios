//
//  AAppointMentExpenseSummaryCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 02/01/22.
//

import UIKit

class AAppointMentExpenseSummaryCell: UITableViewCell {

    @IBOutlet weak var tableviewExpenseDetails: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCellData() {
        tableviewExpenseDetails.reloadData()
    }
}

extension AAppointMentExpenseSummaryCell : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AExpenseDetailsCell", for: indexPath) as! AExpenseDetailsCell;
        //cell.setupCellData(objModel: self.objHomeViewModel)
        
        if(indexPath.row == 0) {
            cell.lblExpenseTypeName.text = "Hotel Cost"
            cell.lblCost.text = "$250.00"
        } else {
            cell.lblExpenseTypeName.text = "Food Cost"
            cell.lblCost.text = "$100.00"
        }
        
        return cell;
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
  }
    
}
