//
//  AAppointMentDateCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 02/01/22.
//

import UIKit

class AAppointMentDateCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellData(value:String) {
        
        let lbl1 = "Rescheduled to:"
        let lbl2 = " Nov 23, 2021 - 10:00 am"
        let targetString = "\(lbl1) \(lbl2)"
        let range = NSMakeRange(lbl1.count+1, lbl2.count)
        lblDate.attributedText = Common.sharedInstance.attributedString(Font1: UIFont(name:"Rubik-Bold",size:14), Font2: UIFont(name:"Rubik-Regular",size:14), from: targetString, nonBoldRange: range, Colour1: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1),Colour2: #colorLiteral(red: 0.09803921569, green: 0.4274509804, blue: 0.05882352941, alpha: 1))
    }

}
