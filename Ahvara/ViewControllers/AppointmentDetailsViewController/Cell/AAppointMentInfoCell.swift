//
//  AAppointMentInfoCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 02/01/22.
//

import UIKit

class AAppointMentInfoCell: UITableViewCell {

    @IBOutlet weak var lblInfo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellData(value:String) {
        
        let lbl1 = "\(value):"
        let lbl2 = " Steve Smith - Oct 20, 2021 | 09:02:15 am - Location"
        let targetString = "\(lbl1) \(lbl2)"
        let range = NSMakeRange(lbl1.count+1 , lbl2.count)
        lblInfo.attributedText = Common.sharedInstance.attributedString(Font1: UIFont(name:"Rubik-Bold",size:11), Font2: UIFont(name:"Rubik-Regular",size:11), from: targetString, nonBoldRange: range, Colour1: #colorLiteral(red: 0.7215686275, green: 0.7215686275, blue: 0.7215686275, alpha: 1),Colour2: #colorLiteral(red: 0.7215686275, green: 0.7215686275, blue: 0.7215686275, alpha: 1))
    }

}
