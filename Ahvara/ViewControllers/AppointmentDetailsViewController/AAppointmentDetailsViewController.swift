//
//  AAppointmentDetailsViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 02/01/22.
//

import UIKit

class AAppointmentDetailsViewController: BaseViewController {
    
    @IBOutlet weak var tableviewAppointmentDetailsList: UITableView!
    @IBOutlet weak var btnStatus: UIButton!
    
    var selectedType : String?
    
    let arrArrived  = [["Title" :"Details", "CellType" :"1"],
                       ["Title" :"Note", "CellType" :"2"],
                       ["Title" :"Rating", "CellType" :"3"],
                       ["Title" :"Leads", "CellType" :"4"],
                       ["Title" :"Workorder", "CellType" :"5"],
                       ["Title" :"Button", "CellType" :"6"],
                       ["Title" :"Mark Arrived", "CellType" :"7"],
                       ["Title" :"Created By", "CellType" :"7"]]
        
    let arrCancel  = [["Title" :"Details", "CellType" :"1"],
                      ["Title" :"Note", "CellType" :"2"],
                      ["Title" :"Rating", "CellType" :"3"],
                      ["Title" :"Leads", "CellType" :"4"],
                      ["Title" :"Workorder", "CellType" :"5"],
                      ["Title" :"Cancel", "CellType" :"8"],
                      ["Title" :"Reason", "CellType" :"9"],
                      ["Title" :"Mark Arrived", "CellType" :"7"],
                      ["Title" :"Created By", "CellType" :"7"]]
    
    let arrCompleted  = [["Title" :"Details", "CellType" :"1"],
                          ["Title" :"Followup", "CellType" :"10"],
                          ["Title" :"Note", "CellType" :"2"],
                          ["Title" :"Rating", "CellType" :"3"],
                          ["Title" :"Leads", "CellType" :"4"],
                          ["Title" :"Expense Summary", "CellType" :"11"],
                          ["Title" :"Workorder", "CellType" :"5"],
                          ["Title" :"Button", "CellType" :"6"],
                          ["Title" :"Report completed by", "CellType" :"7"],
                          ["Title" :"Mark Arrived", "CellType" :"7"],
                          ["Title" :"Created By", "CellType" :"7"]]
    
    let arrReschedule  = [["Title" :"Details", "CellType" :"1"],
                         ["Title" :"Note", "CellType" :"2"],
                         ["Title" :"Rating", "CellType" :"3"],
                         ["Title" :"Leads", "CellType" :"4"],
                         ["Title" :"Workorder", "CellType" :"5"],
                         ["Title" :"Rescheduled to", "CellType" :"12"],
                         ["Title" :"Reason", "CellType" :"9"],
                         ["Title" :"Mark Arrived", "CellType" :"7"],
                         ["Title" :"Created By", "CellType" :"7"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showInitialBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.hideButtonRight3()
        self.setTitleLabelText(text: "Appointment", titleColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconBack"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func actionButtonLeft1() {
        self.navigationController?.popViewController(animated:true)
    }
    
    private func initializeView(){
        tableviewAppointmentDetailsList.rowHeight = UITableView.automaticDimension
        tableviewAppointmentDetailsList.estimatedRowHeight = 40.0
        tableviewAppointmentDetailsList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewAppointmentDetailsList.frame.size.width, height: 100))
        self.tableviewAppointmentDetailsList.tableFooterView = footerView;
        
        
        switch selectedType {
        case "arrived":
            self.btnStatus.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.6039215686, blue: 0.7882352941, alpha: 1)
            self.btnStatus.setTitle("Arrived", for: .normal)
            break
        case "cancel":
            self.btnStatus.backgroundColor = #colorLiteral(red: 0.8745098039, green: 0, blue: 0, alpha: 1)
            self.btnStatus.setTitle("Cancelled", for: .normal)
            break
        case "completed":
            self.btnStatus.backgroundColor = #colorLiteral(red: 0.5333333333, green: 0.7882352941, blue: 0.1921568627, alpha: 1)
            self.btnStatus.setTitle("Completed", for: .normal)
            break
        case "reschedule":
            self.btnStatus.backgroundColor =  #colorLiteral(red: 0.2549019608, green: 0.3019607843, blue: 0.3333333333, alpha: 1)
            self.btnStatus.setTitle("Rescheduled", for: .normal)
            break
        default:
            break
        }
        
        self.loadTableData()
    }
    
    
    // To get the Table data
    private func loadTableData(){
        
        self.tableviewAppointmentDetailsList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
}

extension AAppointmentDetailsViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch selectedType {
        case "arrived":
           return arrArrived.count
        case "cancel":
           return arrCancel.count
        case "completed":
           return arrCompleted.count
        case "reschedule":
           return arrReschedule.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var dict = [String : Any]()
        
        switch selectedType {
        case "arrived":
            dict = arrArrived[indexPath.row]
            break
        case "cancel":
            dict = arrCancel[indexPath.row]
            break
        case "completed":
            dict = arrCompleted[indexPath.row]
            break
        case "reschedule":
            dict = arrReschedule[indexPath.row]
            break
        default:
            break
        }
                
        switch dict["CellType"] as! String {
        case "1":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentDetailsCell", for: indexPath) as! AAppointMentDetailsCell;
            //cell.setupCellData(objModel: self.objHomeViewModel)
            return cell;
        case "2":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentNoteCell", for: indexPath) as! AAppointMentNoteCell;
            //cell.setupCellData(objModel: self.objHomeViewModel)
            return cell;
        case "3":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentRatingCell", for: indexPath) as! AAppointMentRatingCell;
                    //cell.setupCellData(objModel: self.objHomeViewModel)
                    return cell;
        case "4":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentLeadsCell", for: indexPath) as! AAppointMentLeadsCell;
                    //cell.setupCellData(objModel: self.objHomeViewModel)
                    return cell;
        case "5":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentWorkOrdersCell", for: indexPath) as! AAppointMentWorkOrdersCell;
                    //cell.setupCellData(objModel: self.objHomeViewModel)
                    return cell;
        case "6":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentButtonCell", for: indexPath) as! AAppointMentButtonCell;
                    //cell.setupCellData(objModel: self.objHomeViewModel)
                    return cell;
        case "7":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentInfoCell", for: indexPath) as! AAppointMentInfoCell;
                cell.setupCellData(value: dict["Title"] as! String)
                    return cell;
        case "8":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentSimpleLabelCell", for: indexPath) as! AAppointMentSimpleLabelCell;
                    //cell.setupCellData(objModel: self.objHomeViewModel)
                    return cell;
        case "9":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentReasonCell", for: indexPath) as! AAppointMentReasonCell;
                cell.setupCellData(value: dict["Title"] as! String)
                    return cell;
        case "10":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentFollowUpCell", for: indexPath) as! AAppointMentFollowUpCell;
                    //cell.setupCellData(objModel: self.objHomeViewModel)
                    return cell;
        case "11":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentExpenseSummaryCell", for: indexPath) as! AAppointMentExpenseSummaryCell;
                cell.setupCellData()
                    return cell;
        case "12":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AAppointMentDateCell", for: indexPath) as! AAppointMentDateCell;
                cell.setupCellData(value: dict["Title"] as! String)
                    return cell;
        default:
            return UITableViewCell.init()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
    }
    
}
