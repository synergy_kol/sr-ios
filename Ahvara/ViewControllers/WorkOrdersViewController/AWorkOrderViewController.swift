//
//  AWorkOrderViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 21/01/22.
//

import UIKit

class AWorkOrderViewController: BaseViewController {
    
    @IBOutlet var objHomeViewModel: AHomeViewModel!
    @IBOutlet weak var tableviewWorkOrderList: UITableView!
    @IBOutlet weak var txtfieldSearch: UITextField!

    
    var selectedTab : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showHomeBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.showButtonRight1()
        self.showButtonRight2()
        self.hideButtonRight3()
        //let userName = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.username, defaultValue: "")
        self.setTitleLabelText(text: "Work Orders", titleColor: #colorLiteral(red: 0.1529411765, green: 0.3411764706, blue: 0.2980392157, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconMenu"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    private func initializeView(){
        tableviewWorkOrderList.rowHeight = UITableView.automaticDimension
        tableviewWorkOrderList.estimatedRowHeight = 40.0
        tableviewWorkOrderList.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewWorkOrderList.frame.size.width, height: 100))
        self.tableviewWorkOrderList.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData(){
    
        self.tableviewWorkOrderList.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
    @IBAction func actionAddWorkOrder(_ sender: UIButton) {
        let vc:AAddWorkOrderViewController =  Common.getVCWithId(identifier: "AAddWorkOrderViewController", storyBoardName: "Home") as! AAddWorkOrderViewController
            
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension AWorkOrderViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AWorkorderCell", for: indexPath) as! AWorkorderCell;
        
        return cell;
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
      let vc:WorkOrderDetailsViewController =  Common.getVCWithId(identifier: "WorkOrderDetailsViewController", storyBoardName: "Home") as! WorkOrderDetailsViewController
          
      self.navigationController?.pushViewController(vc, animated: true)
  }
}
