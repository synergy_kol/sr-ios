//
//  ASocialMediaCell.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 21/01/22.
//

import UIKit

class ASocialMediaCell: UITableViewCell {

    @IBOutlet weak var txtfieldValue: UITextField!
    @IBOutlet weak var imgviewSocial: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
