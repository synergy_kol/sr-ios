//
//  AMyAccountViewController.swift
//  Ahvara
//
//  Created by Piyali Tarafder on 21/01/22.
//

import UIKit

class AMyAccountViewController: BaseViewController {
    
    @IBOutlet weak var tableviewMyAccount: UITableView!
    @IBOutlet weak var txtfieldSearch: UITextField!

    
    var selectedTab : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NetworkManager.sharedInstance.checkNetworkConnection { (isAvailable, message) in
            Common.sharedInstance.isNetworkEnabled(targetView: self.view, targetedVC: self, message: message, networkEnabled: isAvailable, btnMessage: "")
        }
        self.initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupNavigationBar() {
        self.showHomeBar()
        self.showButtonLeft1()
        self.hideLabelRight1()
        self.showButtonRight1()
        self.showButtonRight2()
        self.hideButtonRight3()
        //let userName = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.username, defaultValue: "")
        self.setTitleLabelText(text: "My Account", titleColor: #colorLiteral(red: 0.1529411765, green: 0.3411764706, blue: 0.2980392157, alpha: 1))
        self.setButtonLeft1Image(image: #imageLiteral(resourceName: "IconMenu"))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    private func initializeView() {
        tableviewMyAccount.rowHeight = UITableView.automaticDimension
        tableviewMyAccount.estimatedRowHeight = 40.0
        tableviewMyAccount.contentInset.bottom = 100
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableviewMyAccount.frame.size.width, height: 100))
        self.tableviewMyAccount.tableFooterView = footerView;
        
        self.loadTableData()
    }
    
    // To get the Table data
    private func loadTableData() {
    
        self.tableviewMyAccount.reloadData()
        
       /* Loader.sharedManager.startAnimatingLoader()
        objHomeViewModel.getAthleteHomeDetails { isSuccess,message  in
                Loader.sharedManager.stopAnimatingLoader()
                if !isSuccess {
                    self.showDefaultAlert(title:"Alert", msg: message)
                }
                DispatchQueue.main.async {
                   
                }
        }*/
    }
    
    @IBAction func actionApply(_ sender: UIButton) {
    }
}

extension AMyAccountViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AProfileImageCell", for: indexPath) as! AProfileImageCell;
            
            return cell;
        case 1,2,3,4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ATextInputCell", for: indexPath) as! ATextInputCell;
            
            if(indexPath.row == 1) {
                cell.txtfieldValue.placeholder = "First Name"
                cell.txtfieldValue.text = "Nicholas"
            } else if(indexPath.row == 2) {
                cell.txtfieldValue.placeholder = "Last Name"
                cell.txtfieldValue.text = "Doyel"
            } else if(indexPath.row == 3) {
                cell.txtfieldValue.placeholder = "Email"
                cell.txtfieldValue.text = "nicholas5132@gmail.com"
            } else if(indexPath.row == 4) {
                cell.txtfieldValue.placeholder = "Phone"
                cell.txtfieldValue.text = "123 456 7890"
            }
            return cell;
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "APasswordCell", for: indexPath) as! APasswordCell;
            
            return cell;
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AHeaderCell", for: indexPath) as! AHeaderCell;
            return cell;
        case 7,8,9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ASocialMediaCell", for: indexPath) as! ASocialMediaCell;
            
            if(indexPath.row == 7) {
                cell.txtfieldValue.text = "Facebook profile link"
                cell.imgviewSocial.image = #imageLiteral(resourceName: "IconFacebook")
            } else if(indexPath.row == 8) {
                cell.txtfieldValue.text = "Linkedin profile link"
                cell.imgviewSocial.image = #imageLiteral(resourceName: "IconLlinkedin")
            } else if(indexPath.row == 9) {
                cell.txtfieldValue.text = "Twitter profile link"
                cell.imgviewSocial.image = #imageLiteral(resourceName: "IconTwitter")
            }
            return cell;
        default:
            return UITableViewCell.init()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
    }
    
}
