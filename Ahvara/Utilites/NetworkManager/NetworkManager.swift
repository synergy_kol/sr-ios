//
//  NetworkManager.swift
//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.
//

import UIKit
import Reachability
//import Reachability


class NetworkManager: NSObject {
    
    static let sharedInstance = NetworkManager()
    var completionBlock: NetworkAvailibilityCompletionBlock?
    let reachability = try! Reachability()
    
    public func initializeReachability() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            debugPrint("could not start reachability notifier")
        }
    }
    
    public func checkNetworkConnection(completionhandler:@escaping NetworkAvailibilityCompletionBlock) {
        self.completionBlock = completionhandler
    }
    
    func isnetworkAvailable() -> Bool {
        return reachability.connection != .unavailable
    }
    
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
        case .wifi:
            debugPrint("Reachable via WiFi")
            self.completionBlock!(true, DefaultMessages.kBackToOnline)
            
        case .cellular:
            debugPrint("Reachable via Cellular")
            
            self.completionBlock!(true, DefaultMessages.kBackToOnline)
            
        case .unavailable:
            debugPrint("Network not reachable")
            
            self.completionBlock!(false, DefaultMessages.kNoNetworkAccess)
        case .none:
            debugPrint("Network not reachable")
            
            self.completionBlock!(false, DefaultMessages.kNoNetworkAccess)
        }
    }

}
