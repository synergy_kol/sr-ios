//
//  PSideMenuViewController.swift
//  Panda
//
//  Created by Piyali Tarafder on 11/10/21.
//

import UIKit

protocol PSideMenuDelegate {
    func didSelectMenuItem()
}

class PSideMenuViewController: UIViewController {

    public var delegate : PSideMenuDelegate?
    
    @IBOutlet weak var tableSideMenu: UITableView!
    
    let data = [["name":"Dashboard","image":"IconSideMenuDashboard"],
                ["name":"Schedule","image":"IconSideMenuCalendar"],
                ["name":"Appointments/Visits","image":"IconSideMenuAppointment"],
                ["name":"Tasks","image":"IconSideMenuTasks"],
                ["name":"Leads","image":"IconSideMenuLead"],
                ["name":"Customers","image":"IconSideMenuCustomer"],
                ["name":"Units","image":"IconSideMenuUnits"],
                ["name":"Work Orders","image":"IconSideMenuWorkOrders"],
                ["name":"Parts","image":"IconSideMenuParts"],
                ["name":"Documents","image":"IconSideMenuDocument"],
                ["name":"Reports","image":"IconSideMenuReports"],
                ["name":"My Account","image":"IconSideMenuAccount"],
                ["name":"Notifications","image":"IconSideMenuNotification"],
                ["name":"Settings","image":""],
                ["name":"Logout","image":""]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.initializeView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(true)
        tableSideMenu.rowHeight = UITableView.automaticDimension
        tableSideMenu.estimatedRowHeight = 40.0
        self.tableSideMenu.reloadData()
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:- UITableView Delegate & DataSource ---
extension PSideMenuViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        if(indexPath.row == 0) {
            let cell : PsideMenuHeaderCell  = tableView.dequeueReusableCell(withIdentifier:"PsideMenuHeaderCell", for: indexPath) as! PsideMenuHeaderCell
           /* let roleid = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.role_id, defaultValue: "")
            let userName = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.username, defaultValue: "")
            let userImg = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.profile_image, defaultValue: "")
            
            
            if let url = URL(string:  BaseUrl.baseUrl + (userImg)) {
                cell.imgviewUserIcon.kf.setImage(
                    with: url,
                    placeholder: #imageLiteral(resourceName: "img2"),
                    options: nil)
                } else {
                cell.imgviewUserIcon.image = #imageLiteral(resourceName: "img2")
            }
            
            cell.lblUsername.text = userName
            cell.lblUserRole.text = roleid*/
            
            return cell
            } else {
        let cell : PSideMenuCell  = tableView.dequeueReusableCell(withIdentifier:"PSideMenuCell", for: indexPath) as! PSideMenuCell
            
                let dict = data[indexPath.row - 1]
                
                cell.lblMenuItemName.text = dict["name"] ?? ""
                cell.imageviewIcon.image = UIImage(named: dict["image"] ?? "")
                
            if (dict["name"] == "Notifications") {
                cell.lblNotificationCount.isHidden = false
            } else {
                cell.lblNotificationCount.isHidden = true
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row > 0) {
            if(indexPath.row == 1) {
                Common.changeRootController(rootControllerType: .RootControllerTypeHome)
            } else if(indexPath.row == 2) {
                Common.changeRootController(rootControllerType: .RootControllerTypeSchedule)
            } else if(indexPath.row == 3) {
                Common.changeRootController(rootControllerType: .RootControllerTypeAppointments)
            }else if (indexPath.row == 4) {
                Common.changeRootController(rootControllerType: .RootControllerTypeTask)
            }else if (indexPath.row == 5) {
                Common.changeRootController(rootControllerType: .RootControllerTypeLeads)
            }else if (indexPath.row == 6) {
                Common.changeRootController(rootControllerType: .RootControllerTypeCustomer)
            }else if (indexPath.row == 7) {
                Common.changeRootController(rootControllerType: .RootControllerTypeUnit)
            }else if (indexPath.row == 8) {
                Common.changeRootController(rootControllerType: .RootControllerTypeWorkOrder)
            }else if (indexPath.row == 9) {
                
            } else if (indexPath.row == 10) {
                Common.changeRootController(rootControllerType: .RootControllerTypeDocuments)
            } else if (indexPath.row == 11){
                Common.changeRootController(rootControllerType: .RootControllerTypeReports)
               
            }else if (indexPath.row == 12){
                Common.changeRootController(rootControllerType: .RootControllerTypeMyAccounts)
                
             }else if (indexPath.row == 13){
                 Common.changeRootController(rootControllerType: .RootControllerTypeNotifications)
             }
            delegate?.didSelectMenuItem()
        }
    }
}
