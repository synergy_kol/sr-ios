//
//  PsideMenuHeaderCell.swift
//  Panda
//
//  Created by Piyali Tarafder on 18/10/21.
//

import UIKit

class PsideMenuHeaderCell: UITableViewCell {

    @IBOutlet weak var imgviewUserIcon: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblUserRole: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
