//
//  PSideMenuCell.swift
//  Panda
//
//  Created by Piyali Tarafder on 11/10/21.
//

import UIKit

class PSideMenuCell: UITableViewCell {

    class var identifier: String { return String(describing: self) }
        class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }

    
    @IBOutlet weak var lblMenuItemName: UILabel!
    @IBOutlet weak var imageviewIcon: UIImageView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
