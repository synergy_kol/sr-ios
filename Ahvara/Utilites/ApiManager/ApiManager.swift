//
//  ApiManager.swift
//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.


import UIKit
import Alamofire
import SwiftyJSON

class ApiManager: NSObject {
    
    static let sharedInstance = ApiManager()
    public var searchRequest: DataRequest?
    
    /* class func login(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()) {
        
        let url = BaseUrl.TbaseUrl + ApiEndPoints.TSignIn
        //let headers: HTTPHeaders = [:]
        
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            debugPrint("Response=",response.result)
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
        }
    }*/
    
    //MARK:- Update Device Token ---
    /*class func updateDeviceToken(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()) {
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        let url = BaseUrl.TbaseUrl + ApiEndPoints.TUpdateDeviceToken
        
        AF.request(url,method: .post,parameters: parameters,headers: header).responseJSON { response in
            completionhandler(response.value as Any,response.error)
        }
    }*/
    
    // MARK:- Login
    
   /*  class func login(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()) {
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PLogin
        //let headers: HTTPHeaders = [:]
        
        AF.request(url, method: .post, parameters: parameters).responseJSON { response in
          debugPrint(response)
            completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Send OTP For Forgot Password ---
    class func sendOTPForForgetPassword(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
        let url = BaseUrl.baseUrl + APIUrlConstants.PSendOTPForForgotPwd
        
        AF.request(url,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Verify OTP For Forgot Password ---
    class func verifyOTPForForgetPassword(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
        let url = BaseUrl.baseUrl + APIUrlConstants.PVerifyOTPForForgotPwd
        
        AF.request(url,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Verify OTP For Forgot Password ---
    class func resetPassword(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
        let url = BaseUrl.baseUrl + APIUrlConstants.PResetPassword
        
        AF.request(url,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
   
    
    //MARK:- Get Sports List Data ---
    class func getSportsList(completionhandler :@escaping (SportsListModel?, Error?) -> ()){
        let url = BaseUrl.baseUrl + APIUrlConstants.PGetSports
    
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding(destination: .httpBody))
        .responseSportsListModel{ response in
            if let apiDataModel = response.value {
                completionhandler(apiDataModel,nil)
            }else{
                print(response.error.debugDescription)
                completionhandler(nil,response.error)
            }
        }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get College List Data ---
    class func getCollegeList(completionhandler :@escaping (CollegeListModel?, Error?) -> ()){
        //let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        let url = BaseUrl.baseUrl + APIUrlConstants.PGetColleges
    
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding(destination: .httpBody))
        .responseCollegeListModel{ response in
           // print(response.value)
            if let apiDataModel = response.value {
                completionhandler(apiDataModel,nil)
            }else{
                print(response.error.debugDescription)
                completionhandler(nil,response.error)
            }
        }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get County List Data ---
    
    class func getCountyList(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (CountyListModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetStates
        
        AF.request(url, method: .get , parameters: parameters ,encoding: URLEncoding.default)
        .responseCountyListModel{ response in
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              } else {
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get State List Data ---
    class func getStateList(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (StateListModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetStates
        
        AF.request(url, method: .get , parameters: parameters ,encoding: URLEncoding.default)
        .responseStateListModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get City List Data ---
    class func getCityList(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (CityListModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetCity
        
        AF.request(url, method: .get , parameters: parameters ,encoding: URLEncoding.default)
        .responseCityListModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- School Playing Level List Data ---
    class func getSchoolPlayingLevelList(completionhandler :@escaping (SchoolPlayingLevelModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetSchoolPlayingLevel
        
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding.default)
        .responseSchoolPlayingLevelModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Education Level List Data ---
    class func getEducationLevelList(completionhandler :@escaping (EducationLevelModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetEducationLevel
        
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding.default)
        .responseEducationLevelModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Club List Data ---
    class func getClubList(completionhandler :@escaping (ClubListModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetClubs
        
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding.default)
        .responseClubListModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    
    //MARK:- Get Verify Email ---
    
    class func verifyEmailId(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (VerifyEmailModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PVerifyEmail
        
        AF.request(url, method: .post , parameters: parameters ,encoding: URLEncoding.default)
        .responseVerifyEmailModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    
    //MARK:- Get Verify PhoneNumber ---
    class func verifyPhoneNumber(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (VerifyPhoneNumberModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PVerifyMobile
        
        AF.request(url, method: .post , parameters: parameters ,encoding: URLEncoding.default)
        .responseVerifyPhoneNumberModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Submit Athlete Basic Info ---

    class func submitAthleteBasicInfo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
    
        let url = BaseUrl.baseUrl + APIUrlConstants.PSubmitBasicInfoAthelete
        
        AF.request(url,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
   
    //MARK:- Submit Athlete Address Info ---
    class func submitAthleteAddressInfo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
    
        var strUrl : String!
        
        if(Common.sharedInstance.selectedAccountType == .athlete) {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PSubmitAddressInfoAthelete
        } else {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PSubmitAddressInfoCoach
        }
        
        let url = strUrl
        
        AF.request(url!,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Submit Athlete Physical Info ---
    class func submitAthletePhysicalInfo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
        let url = BaseUrl.baseUrl + APIUrlConstants.PSubmitAthletePhysicalInfo
        
        AF.request(url,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Submit Sports Selection Info ---
    class func submitSportsSelectionInfo(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (SportsDetailModel?, Error?) -> ()){

        var strUrl : String!
        
        if(Common.sharedInstance.selectedAccountType == .athlete) {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PSubmitSelctSportsInfo
        } else {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PSubmitSportsInfoCoach
        }
        
        let url = strUrl
        
        
        AF.request(url!, method: .post , parameters: parameters ,encoding: URLEncoding.default)
        .responseSportsDetailModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get Sport Position List Data ---
    class func getSportPositionList(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (SportsPositionModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetSportPositionList
        
        AF.request(url, method: .get , parameters: parameters ,encoding: URLEncoding.default)
        .responseSportsPositionModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Submit Sports Positions Info ---
    class func submitSportsPositionSelectionInfo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
        let url = BaseUrl.baseUrl + APIUrlConstants.PGetSportPositionSelectionInfo
        
        AF.request(url,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Submit College selection Info ---
    class func submitCollegeSelectionInfo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
        
        var strUrl : String!
        
        if(Common.sharedInstance.selectedAccountType == .athlete) {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PGetCollegeSelectionInfo
        } else {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PGetCollegeSelectionInfoCoach
        }
        
        let url = strUrl
                
        AF.request(url!,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Submit Coach Basic Info ---
    
    class func submitCoachBasicInfo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
    
        let url = BaseUrl.baseUrl + APIUrlConstants.PSubmitBasicInfoCoach
        
        AF.request(url,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Get Preference List Data ---
    class func getPreferenceList(completionhandler :@escaping (PreferenceModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetPreference
        
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding.default)
        .responsePreferenceModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Submit Coach Other Info ---
    class func submitCoachOtherInfo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
            
        let url = BaseUrl.baseUrl + APIUrlConstants.PSubmitOtherInfoCoach
                
        AF.request(url,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Submit Guardian Info ---
    class func submitGuardianInfo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
            
        let url = BaseUrl.baseUrl + APIUrlConstants.PSubmitGuardianInfo
        
        AF.request(url,method: .post,parameters: parameters).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Get School competition Level List Data ---
    class func getCompetitionList(completionhandler :@escaping (CompetitionLevelModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetCompetitionLevel
        
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding.default)
        .responseCompetitionLevelModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- get Subscription Info ---
    class func getSubscriptionInfo(completionhandler :@escaping (SubscriptionDataModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetSubscriptionInfo
        
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding.default)
        .responseSubscriptionDataModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- get Workout Category List ---
    class func getWorkoutCategoryList(completionhandler :@escaping (WorkoutCategoryModel?, Error?) -> ()){
        //let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        let url = BaseUrl.baseUrl + APIUrlConstants.PGetWorkoutCategoryList
    
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding(destination: .httpBody))
        .responseWorkoutCategoryModel{ response in
           // print(response.value)
            if let apiDataModel = response.value {
                completionhandler(apiDataModel,nil)
            }else{
                print(response.error.debugDescription)
                completionhandler(nil,response.error)
            }
        }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get Category Details List Data ---
    class func getWorkoutCategoryDetailsList(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (WorkoutCategoryDetailsModel?, Error?) -> ()){

        
        let url = BaseUrl.baseUrl + APIUrlConstants.PGetWorkoutCategoryDetailsList
        
        AF.request(url, method: .get , parameters: parameters ,encoding: URLEncoding.default)
        .responseWorkoutCategoryDetailsModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Submit Workout Category Library List Data ---
    class func submitSaveWorkoutsLibraryInfo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
           
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PSubmitWorkoutCategoryLibrary
        
        AF.request(url,method: .post,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Get Category Tips List Data ---
    class func getWorkoutCategoryVideoTipsList(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (WorkoutCategoryTipsModel?, Error?) -> ()){

        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PGetWorkoutCategoryTipsList
        
        AF.request(url, method: .get , parameters: parameters, encoding: URLEncoding.default, headers: header)
        .responseWorkoutCategoryTipsModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get Event Details Data ---
    class func getEventDetailsList(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (EventDetailsModel?, Error?) -> ()){

        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PEventDetails
        
        AF.request(url, method: .get , parameters: parameters, encoding: URLEncoding.default, headers: header)
        .responseEventDetailsModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Submit Event Info ---
    class func submitEventInfo(isEdit: Bool,parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
            
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        var strUrl : String!
        
        if(isEdit) {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PEventUpdate
        } else {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PEventInfo
        }
        
        let url = strUrl
                
        AF.request(url!,method: .post,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Get Event List Data ---
    class func getEventList(isOpportunities: Bool, completionhandler :@escaping (EventListModel?, Error?) -> ()){
        
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        var strUrl : String!
        
        if(isOpportunities) {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PEventOpportunitiesInfo
        } else {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PEventInfo
        }
    
        AF.request(strUrl!, method: .get , parameters: nil ,encoding: URLEncoding(destination: .httpBody), headers: header)
        .responseEventListModel{ response in
            if let apiDataModel = response.value {
                completionhandler(apiDataModel,nil)
            }else{
                print(response.error.debugDescription)
                completionhandler(nil,response.error)
            }
        }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Delete Event details ---
    class func deleteEvent(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
           
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PEventDetailsDelete
        
        AF.request(url,method: .get,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Submit Game info data ---
    class func submitGameInfo(isEdit: Bool, parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
            
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        var strUrl : String!
        
        if(isEdit) {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PGameUpdate
        } else {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PGameInfo
        }
        
        let url = strUrl
                
        AF.request(url!,method: .post,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Get Event List Data ---
    class func getGameHighlightsList(completionhandler :@escaping (GameHighlightsListModel?, Error?) -> ()){
        
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PGameInfo
    
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding(destination: .httpBody), headers: header)
        .responseGameHighlightsListModel{ response in
            if let apiDataModel = response.value {
                completionhandler(apiDataModel,nil)
            }else{
                print(response.error.debugDescription)
                completionhandler(nil,response.error)
            }
        }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get Game Details Data ---
    class func getGameDetailsList(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (GameDetailsModel?, Error?) -> ()){

        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PGameDetails
        
        AF.request(url, method: .get , parameters: parameters, encoding: URLEncoding.default, headers: header)
        .responseGameDetailsModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Delete Game details ---
    class func deleteGame(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
           
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PGameDetailsDelete
        
        AF.request(url,method: .get,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Get Event category List Data ---
    class func getEventCategoryList(completionhandler :@escaping (EventCategoryModel?, Error?) -> ()){
        
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetEventCategoryList
        
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding.default, headers: header)
        .responseEventCategoryModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get Video Evidence List Data ---
    class func getVideoEvidenceList(completionhandler :@escaping (VideoEvidanceListModel?, Error?) -> ()){
        
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PVideoEvidenceInfo
    
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding(destination: .httpBody), headers: header)
        .responseVideoEvidanceListModel{ response in
            if let apiDataModel = response.value {
                completionhandler(apiDataModel,nil)
            }else{
                print(response.error.debugDescription)
                completionhandler(nil,response.error)
            }
        }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Delete Video details ---
    class func deleteVideo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
           
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PVideoEvidenceDelete
        
        AF.request(url,method: .get,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Get Video Details Data ---
    class func getVideoDetailsList(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (VideoDetailsModel?, Error?) -> ()){

        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PVideoEvidenceDetails
        
        AF.request(url, method: .get , parameters: parameters, encoding: URLEncoding.default, headers: header)
        .responseVideoDetailsModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                  completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Submit Video info data ---
    class func submitVideoInfo(isEdit: Bool, parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
            
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        var strUrl : String!
        
        if(isEdit) {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PVideoUpdate
        } else {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PVideoEvidenceInfo
        }
        
        let url = strUrl
                
        AF.request(url!,method: .post,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Get Group List Data ---
    class func getGroupList(completionhandler :@escaping (TeamingUpGroupListModel?, Error?) -> ()){
        
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PTeamingGroupInfo
    
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding(destination: .httpBody), headers: header)
        .responseTeamingUpGroupListModel{ response in
            if let apiDataModel = response.value {
                completionhandler(apiDataModel,nil)
            }else{
                print(response.error.debugDescription)
                completionhandler(nil,response.error)
            }
        }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get Group Details Data ---
    class func getGroupDetails(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (TeamingUpGroupDetailsModel?, Error?) -> ()){

        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PTeamingGroupDetails
        
        AF.request(url, method: .get , parameters: parameters, encoding: URLEncoding.default, headers: header)
        .responseTeamingUpGroupDetailsModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get Group Invite member List Data ---
    class func getGroupInviteMemberList(parameters: Dictionary<String,AnyObject>, completionhandler :@escaping (GroupInviteMemberlistModel?, Error?) -> ()){

        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PTeamingGroupInviteMemberList
        
        AF.request(url, method: .get , parameters: parameters, encoding: URLEncoding.default, headers: header)
        .responseGroupInviteMemberlistModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Submit Video info data ---
    class func inviteNewMember(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
            
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PTeamingGroupSendInviteMember
                
        AF.request(url,method: .post,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Get Coachin Level List Data ---
    class func getCoachinList(completionhandler :@escaping (CoachinLevelModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetCoachinLevel
        
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding.default)
        .responseCoachinLevelModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get Sport Level List Data ---
    class func getSportList(completionhandler :@escaping (SportLevelModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetSportLevel
        
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding.default)
        .responseSportLevelModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get Dominant Level List Data ---
    class func getDominantList(completionhandler :@escaping (DominantLevelModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetDominantLevel
        
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding.default)
        .responseDominantLevelModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Upload IMAGE FOR MY SERVICE
    
    
    
    class func submitGroupDetailsWithImage(parameters: Dictionary<String, AnyObject>, imageData: Data?, completionhandler:@escaping (Any?, Error?) -> ()) {
        
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PTeamingGroupInfo
       
        
        AF.upload(multipartFormData: { multiPart in
            for (key, value) in parameters {
                multiPart.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }

            if let imgdata = imageData {
                multiPart.append(imgdata, withName: ApiKeyConstants.PImage, fileName: "image.png", mimeType: "image/png")
            }
        }, to: url, method: .post, headers: header)
            .uploadProgress(queue: .main, closure: { progress in
                //Current upload progress of file
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { data in
                //Do what ever you want to do with response
                print(data)
                switch data.result{
                    case .success:
                        completionhandler(data.value as Any,data.error)
                    case .failure(_):
                        completionhandler(data.result as Any,data.error)
                }
            })
        
        
        
        
       /* var params = parameters
        params.removeValue(forKey: "image")
        
        var image : UIImage!
        
        let fileName = Common.sharedInstance.getStringValueFromAPIValue(apiValue: parameters["image"], defaultValue: "")
        if !(fileName.isEmpty) {
            image = Common.sharedInstance.getImageFromDocumentDirectory(imageName: fileName )
        }
        
            AF.upload(multipartFormData: { multiPart in
                for (key, value) in parameters {
                    
                    if key == "image" {
                        if(image != nil) {
                            multiPart.append(image.jpegData(compressionQuality: 1)!, withName: "image" , fileName: fileName, mimeType: "image/jpeg")
                        }
                    } else {
                        if let temp = value as? String {
                            multiPart.append(temp.data(using: .utf8)!, withName: key)
                        }
                        if let temp = value as? Int {
                            multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
                        }
                        if let temp = value as? NSArray {
                            temp.forEach({ element in
                                let keyObj = key + "[]"
                                if let string = element as? String {
                                    multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                                } else
                                    if let num = element as? Int {
                                        let value = "\(num)"
                                        multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                                }
                            })
                        }
                    }
                }
                
            }, to: url, method: .post , headers: header)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .responseJSON(completionHandler: { response in
                    //Do what ever you want to do with response
                    if let error = response.error {
                        completionhandler(response.result as Any,error)
                    }
                    guard let data = response.value else {
                        return
                    }
                    completionhandler(data as Any,response.error)
                })*/
    }
    
    //MARK:- Submit Video info data ---
  /*  class func submitGroupDetailsWithImage(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
            
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PTeamingGroupInfo
                
        AF.request(url,method: .post,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }*/
    
    //MARK:- Get AthleteHome Details Data ---
    class func getAthleteHomeDetailsData(completionhandler :@escaping (AthleteHomeModel?, Error?) -> ()){

        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PGetAthleteHome
        
        AF.request(url, method: .get , parameters: nil, encoding: URLEncoding.default, headers: header)
        .responseAthleteHomeModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  print(response.error.debugDescription)
                completionhandler(nil,response.error)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Update Workouts Library Data ---
    
    class func updateWorkoutsLibraryInfo(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
           
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PUpdateWorkoutLibrary
        
        AF.request(url,method: .post,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
        }
    }
    
    //MARK:- Get Profile Details Data ---
    class func getProfileDetailsData(parameters: Dictionary<String, AnyObject>,completionhandler :@escaping (ProfileModel?, Error?) -> ()){

        let url = BaseUrl.baseUrl + APIUrlConstants.PGetProfileDetails
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
        .responseProfileModel{ response in
            
            if let apiDataModel = response.value {
                  completionhandler(apiDataModel,nil)
              }else{
                  completionhandler(nil,response.error)
                  print(response.error.debugDescription)
              }
         }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Get Connection List Data ---
    class func getConnectionList(completionhandler :@escaping (ConnectionListModel?, Error?) -> ()){
        
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PGetChatList
    
        AF.request(url, method: .get , parameters: nil ,encoding: URLEncoding(destination: .httpBody), headers: header)
        .responseConnectionListModel{ response in
            if let apiDataModel = response.value {
                completionhandler(apiDataModel,nil)
            }else{
                print(response.error.debugDescription)
                completionhandler(nil,response.error)
            }
        }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Delete Video details ---
    class func followUnfollowSpecificUser(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
           
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PApiFollowUnfollow
        
        AF.request(url,method: .post,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }
    
    //MARK:- Get Saved Compare List Data ---
    class func getCompareGroupList(completionhandler :@escaping (SavedCompareListModel?, Error?) -> ()){
        
        let userid = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.userid, defaultValue: "")
        
        let params = [ApiKeyConstants.PUserId : userid] as Dictionary<String, AnyObject>
        
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PGetSaveComapreGroupList
    
        AF.request(url, method: .post , parameters: params ,encoding: URLEncoding(destination: .httpBody), headers: header)
        .responseSavedCompareListModel{ response in
            if let apiDataModel = response.value {
                completionhandler(apiDataModel,nil)
            }else{
                print(response.error.debugDescription)
                completionhandler(nil,response.error)
            }
        }.responseString { (strResponse) in
            print(strResponse.value ?? "")
        }
    }
    
    //MARK:- Submit Comparison List data ---
    
    class func submitToComparisonList(isAtheleteComparison: Bool, parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
           
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        var strUrl : String!
        
        if(isAtheleteComparison) {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PAddToComparisonList
        } else {
            strUrl = BaseUrl.baseUrl + APIUrlConstants.PAddToComparisonGroupList
        }
        
        let url = strUrl
        
        AF.request(url!,method: .post,parameters: parameters, headers: header).responseJSON { response in
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
        }
    }
    
    class func deleteCompare(parameters: Dictionary<String, AnyObject>, completionhandler:@escaping (Any?, Error?) -> ()){
           
        let accessToken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.accesstoken, defaultValue: "")
        
        let header: HTTPHeaders = [
            "Authorization"      : "Bearer \(accessToken)"
        ]
        
        let url = BaseUrl.baseUrl + APIUrlConstants.PDeleteComparison
        
        AF.request(url,method: .get,parameters: parameters, headers: header).responseJSON { response in
            
            switch response.result{
                case .success:
                    completionhandler(response.value as Any,response.error)
                case .failure(_):
                    completionhandler(response.result as Any,response.error)
            }
            //completionhandler(response.value as Any,response.error)
        }
    }*/
    
}



