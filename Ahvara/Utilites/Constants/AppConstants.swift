//
//  AppConstants.swift
//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.
//

import UIKit
import GoogleMaps

//Lifeple Customer - com.lifeple.customer
//Lifeple Service Provider - com.lifeple.provider

struct ApiKeyConstants {
    static let PStatus = "status"
    static let PMessage = "message"
    static let PFaliure = "Faliure"
    static let PSuccess = "Success"
    static let PDeviceToken = "device_token"
    static let PDeviceType = "device_type"
    static let PEmail = "email"
    static let PPassword = "password"
    static let PConfirm_Password = "confirm_password"
    static let PConfirmPassword = "confirmPassword"
    static let PCurrentPassword = "currentPassword"
    static let POld_Password = "old_password"
    static let PNew_Password = "new_password"
    static let PNewPassword = "newPassword"
    static let PMobile = "mobile"
    static let PUserName = "username"
    static let PUser_Name = "user_name"
    static let PUserId = "user_id"
    static let PProfileImage = "picture"
    static let PGender = "gender"
    static let PDOB = "dob"
    static let PYear = "year"
    static let PGraduationYear = "garduationyear"
    static let PMonth = "month"
    static let PDay = "day"
    static let PGraduation_Year = "graduation_year"
    static let PDateOfBirth = "date_of_year"
    static let PPhoneNumber = "phone_no"
    static let PAddress = "address"
    static let PProfileType = "profile_type"
    static let PState = "state"
    static let PCity = "city"
    static let PCounty = "county"
    static let PZipCode = "zip"
    static let PCountry_Id = "country_id"
    static let PCounty_Id = "county_id"
    static let PState_Id = "state_id"
    static let PCity_Id = "city_id"
    static let PWeight = "weight"
    static let PHeight = "height"
    static let PWingSpan = "wingspan"
    static let PHead = "head"
    static let PHandMeasure = "handmeasure"
    static let PDominanthand = "dominanthand"
    static let PDominantfoot = "dominantfoot"
    static let PDominant_hand = "dominant_hand"
    static let PDominant_foot = "dominant_foot"
    static let PEducation_Id = "education_id"
    static let PGrade = "grade"
    static let PEducationlevel = "educationlevel"
    static let PSchoolplayinglevel = "schoolplayinglevel"
    static let PCompetitivelevel = "competitivelevel"
    static let PClub = "club"
    static let PSports = "sports"
    static let PColleges = "colleges"
    static let PSport_Id = "sport_id"
    static let PSport_Positions = "sport_positions"
    static let PSchool_level_id = "school_level_id"
    static let PCompetition_level_id = "competition_id"
    static let POther_level_id = "other_level_id"
    static let PCoachinlevel = "coachinlevel"
    static let PSport = "sport"
    static let PClubOrSchool = "cluborschool"
    static let Pbio = "bio"
    static let PBioLink = "biolink"
    static let PProfile = "profile"
    static let PAllowAccess = "allowaccess"
    static let PPreference = "preference"
    static let PCoachingLevel = "coachinglevel"
    static let PCoaching_sport = "coaching_sport"
    static let PSport_Level = "sport_level"
    static let PTeam_Name = "team_name"
    static let PMulSport = "multiplesport"
    
    static let PHeight_Feet = "height_feet"
    static let PHeight_Inch = "height_inch"
    static let PWingSpan_Feet = "wingspan_feet"
    static let PWingSpan_Inch = "wingspan_inch"
    
    static let PCollege_id = "college_id"
    static let PCoaching_level = "coaching_level"
    static let PSchool = "school"
    static let PCoach_level_name = "coach_level_name"
    static let PGender_of_coaching = "gender_of_coaching"
    static let PPreference_id = "preference_id"
    static let PServe_as_reference = "serve_as_reference"
    static let PAbout = "about"
    static let PAbout_link = "about_link"
    
    static let PRelationship = "relationship"
    static let PFirst_name = "first_name"
    static let PLast_name = "last_name"
    static let PEnable_textmessage = "enable_textmessage"
    static let PPrimary_phone = "primary_phone"
    static let PPrimary_phone_type = "primary_phone_type"
    static let PIs_primary_text = "is_primary_text"
    static let PSecondary_phone = "secondary_phone"
    static let PSecondary_phone_type = "secondary_phone_type"
    static let PIs_secondary_text = "is_secondary_text"
    static let PPrimary_email = "primary_email"
    
    static let PFirstname = "firstname"
    static let PLastname = "lastname"
    static let PCheckbox = "checkbox"
    static let PPrimaryphone = "primaryphone"
    static let PPrimaryphonetype = "primaryphonetype"
    static let PPrimarytextcheck = "primarytextcheck"
    static let PSecondaryphone = "secondaryphone"
    static let PSecondaryphonetype = "secondaryphonetype"
    static let PSecondarytextcheck = "secondarytextcheck"
    
    static let PWorkout_category_id = "workout_category_id"
    static let PWorkout_id = "workout_id"
    static let PWorkout_library = "workout_library"
    static let PWorkoutCategory = "workoutcategory"
    static let PWorkoutType = "workouttype"
    static let PWorkout_type_id = "workout_type_id"
    static let PWorkout_library_id = "workout_library_id"
    
    static let PEventCategory = "eventcategory"
    static let PEventName = "eventname"
    static let PEventLocation = "eventlocation"
    static let PStartdate = "startdate"
    static let PEnddate = "enddate"
    static let PEventNote = "eventnote"
    static let PEventNoteLink = "eventnotelink"
    static let PEventDetail = "eventdetail"
    
    static let PEvent_Name = "event_name" //:Test event 21
    static let PLocation = "location" //:Test
    static let PStart_Date = "start_date" //:10/11/2021
    static let PEnd_Date = "end_date" //:10/12/2021
    static let PEvent_Note = "even_note" //:Test Note
    static let PEvent_Details = "even_details" //:Test descriptions
    static let PCategory = "category" //:2
    static let PEvent_id = "event_id"
    
    static let PDate = "date"
    static let PVideoLink = "videolink"
    static let PRecordDate = "record_date"
    static let PDate_of_video = "date_of_video"
    static let PDescription = "description"
    static let PVideo = "video"
    static let PGame_id = "game_id"
    static let PVideo_Evidence_id = "video_evidence_id"
    static let PVideo_Link = "video_link"
    
    static let PGroup_id = "group_id"
    static let PMemberId = "userId"
    static let PGroupname = "groupname"
    static let PGroupdescription = "groupdescription"
    static let PGroup_name = "group_name"
    static let PGroup_user_id = "group_user_id"
    static let PImage = "image"
    static let PProfile_Image = "profile_image"
    
    static let PLinkViaChat = "linkviachat"
    static let PLinkShare = "linkshare"
    static let PLinkCopy = "linkcopy"
    
    static let PHeader = "header"
    static let PLinkHeader = "linkheader"
    static let PUnit1 = "unit1"
    static let PUnit2 = "unit2"
    static let PUnit_1 = "unit_1"
    static let PUnit_2 = "unit_2"
    static let PComparisonName = "comparison_name"
    static let PCompareGrp = "compare_grp"
    static let PGroupComparisonName = "group_comparison_name"
    static let PGroupCompare = "group_compare"
    static let PCompareId = "compareId"
    static let PUrl = "url"
    
    
    
}


struct Credential {

    static let GoogleClientID               : String = ""
    static let GoogleApiKey                 : String = ""
}

struct APPURL {
    static let appUrlDev : String = "https://dev.fitser.com/dev2/Panda/dev/"
    static let appUrlLive : String = ""
}

struct BaseUrl {
    static let baseUrl : String = APPURL.appUrlDev
}

struct APIUrlConstants {
    //MARK:- BASE Url -
    static let PLogin                                   : String = "api/login"
    static let PGetSports                               : String = "api/get-sports"
    static let PGetColleges                             : String = "api/get-colleges"
    static let PGetCountries                            : String = "api/get-country"
    static let PGetStates                               : String = "api/get-state"
    static let PGetCity                                 : String = "api/get-city"
    static let PSendOTPForForgotPwd                     : String = "api/forgot-password"
    static let PVerifyOTPForForgotPwd                   : String = "api/validate-otp"
    static let PResetPassword                           : String = "api/update-password"
    static let PVerifyMobile                            : String = "api/registration/verify-mobile"
    static let PVerifyEmail                             : String = "api/registration/verify-email"
    static let PGetSchoolPlayingLevel                   : String = "api/get-school-playing-level"
    static let PGetEducationLevel                       : String = "api/get-education-level"
    static let PGetClubs                                : String = "api/get-clubs"
    static let PGetPreference                           : String = "api/get-preference"
    static let PSubmitBasicInfoAthelete                 : String = "api/registration/athlete/step-1"
    static let PSubmitAddressInfoAthelete               : String = "api/registration/athlete/step-2"
    static let PSubmitAthletePhysicalInfo               : String = "api/registration/athlete/step-3"
    static let PSubmitSelctSportsInfo                   : String = "api/registration/athlete/step-4"
    static let PGetSportPositionSelectionInfo           : String = "api/registration/athlete/step-5"
    static let PGetCollegeSelectionInfo                 : String = "api/registration/athlete/step-6"
    static let PGetSportPositionList                    : String = "api/get-sports-positions"
    static let PSubmitBasicInfoCoach                    : String = "api/registration/coach/step-1"
    static let PSubmitAddressInfoCoach                  :String = "api/registration/coach/step-2"
    static let PSubmitOtherInfoCoach                    : String = "api/registration/coach/step-3"
    static let PSubmitSportsInfoCoach                   : String = "api/registration/coach/step-4"
    static let PGetCollegeSelectionInfoCoach            : String = "api/registration/coach/step-5"
    static let PSubmitGuardianInfo                      : String = "api/registration/athlete/save-guardian-info"
    static let PGetCompetitionLevel                     : String = "api/get-competition-level"
    static let PGetSubscriptionInfo                     : String = "api/get-subscription?type=1"
    static let PGetWorkoutCategoryList                  : String = "api/get-workouts"
    static let PGetWorkoutCategoryDetailsList           : String = "api/get-workouts/library"
    static let PSubmitWorkoutCategoryLibrary            : String = "api/save-workouts-library"
    static let PGetWorkoutCategoryTipsList              : String = "api/get-workouts-tips"
    static let PEventInfo                               : String = "api/events"
    static let PEventOpportunitiesInfo                  : String = "api/event-opportunities"
    static let PEventDetails                            : String = "api/events-details"
    static let PEventDetailsDelete                      : String = "api/events-delete"
    static let PEventUpdate                             : String = "api/events-update"
    static let PGameInfo                                : String = "api/games"
    static let PGameUpdate                              : String = "api/games-update"
    static let PGameDetails                             : String = "api/games-details"
    static let PGameDetailsDelete                       : String = "api/games-delete"
    static let PGetEventCategoryList                    : String = "api/get-events-category"
    static let PVideoEvidenceInfo                       : String = "api/video-evidence"
    static let PVideoEvidenceDelete                     : String = "api/video-evidence-delete"
    static let PVideoEvidenceDetails                    : String = "api/video-evidence-details"
    static let PVideoUpdate                             : String = "api/video-evidence-update"
    
    static let PTeamingGroupInfo                        : String = "api/teaming-group"
    static let PTeamingGroupDetails                     : String = "api/teaming-group-details"
    static let PTeamingGroupInviteMemberList            : String = "api/teaming-group/get-invite-list"
    static let PTeamingGroupSendInviteMember            : String = "api/teaming-group/send-invite"
    static let PGetCoachinLevel                         : String = "api/coaching-level"
    static let PGetSportLevel                           : String = "api/sport-level"
    static let PGetDominantLevel                        : String = "api/registration/athlete/dominant"
    static let PGetAthleteHome                          : String = "api/athlete-dashboard-api"
    static let PUpdateWorkoutLibrary                    : String = "api/save-workout-exercise"
    static let PGetProfileDetails                       : String = "api/athlete-profile-api"
    static let PGetChatList                             : String = "api/athlete-chatlist-api"
    static let PApiFollowUnfollow                       : String = "api/add-follow-api"
    static let PGetSaveComapreGroupList                 : String = "api/save-compare-group-list-api"
    static let PAddToComparisonList                     : String = "api/add-group-api"
    static let PAddToComparisonGroupList                : String = "api/add-group-comparison-api"
    static let PDeleteComparison                        : String = "api/delete-comparison-api"
    
}

struct DefaultMessages {
    
    static let kNetworkError                    : String = "Please Check Your Internet Connection."
    static let kNoNetworkAccess                 : String = "No Network Access."
    static let kBackToOnline                    : String = "Back To Online."
    static let kAllowLocationService            : String = "Allow Your Location Service."

    static let warning                          : String = "Warning";
    static let error                            : String = "Message";
    static let success                          : String = "Success";
    static let somethingWrong                   : String = "Something went wrong. Please try again later.";
    static let pleaseTryAgain                   : String = "Please try again";
    static let pleaseWait                       : String = "Please wait";
    static let timeOut                          : String = "Time out";
    static let underConstruction                : String = "Note";

    static let emptyEmailOrPhone                : String = "Please enter email address or phone number"
    static let validEmailOrPhone                : String = "Please enter valid email address or phone number"
    static let validPhoneNumber                 : String = "Please enter valid phone number"
    static let emptyFirstName                   : String = "Please enter first name"
    static let validUsername                    : String = "Please enter valid username"
    static let emptyUsername                    : String = "Please enter username"
    static let emptyLastName                    : String = "Please enter last name."
    static let emptyEmailAddress                : String = "Please enter email address"
    static let validEmail                       : String = "Please enter a valid email address"
    static let verifyEmail                      : String = "Please verify your email address"
    static let emptyPhoneNumber                 : String = "Please enter phone number"
    static let verifyPhoneNumber                : String = "Please verify your mobile number"
    static let emptyPassword                    : String = "Please enter password"
    static let validPassword                    : String = "Password should contain atleast 8 characters"
    static let mismatchPassword                 : String = "Confirm Password is not valid."
    static let validZipCode                     : String = "Please enter valid postal code"
    static let emptyOTP                         : String = "Please enter a OTP"
    static let validOTP                         : String = "Please enter a valid OTP"
    static let titleReset                       : String = "Clear all"
    static let messageReset                     : String = "Do you want to clear applied filters ?"
    static let logout                           : String = "Logout";
    static let logoutMessage                    : String = "Are you sure you want to sign out?"
    static let delete                           : String = "Delete";
    static let remove                           : String = "Remove";
    static let address                          : String = "Address";
    static let eventDeleteMessage               : String = "Are you sure you want to delete this event?"
    static let gameDeleteMessage               : String = "Are you sure you want to delete this game?"
    static let addressMessage                   : String = "Are you sure you want to delete this address?"
    static let order                            : String = "Order";
    static let orderMessage                     : String = "Are you sure you want to cancel this order?"
    static let deleteDefaultAddress             : String = "You can't delete default addess"
    static let validNewEmail                    : String = "Email should not be same to the previous email"
    static let underDevelopmentMessage          : String = "This page is under development"
    static let selectTermsAndCondition          : String = "Please Accept Terms And Condition "
    static let compareDeleteMessage             : String = "Are you sure you want to delete this compare details?"
    

    static let photobook                        : String = "Photobook";
    static let photobookMessage                 : String = "Are you sure you want to delete this photobook?"

    static let emptyState                       : String = "Please select state"
    static let emptyCity                        : String = "Please select city"
    static let emptyCounty                      : String = "Please select county"
    static let emptyZipCode                     : String = "Please enter zipcode"
    
    static let emptyHeight                      : String = "Please enter height"
    static let emptyHeightFeet                  : String = "Please enter height feet"
    static let emptyHeightInch                  : String = "Please enter height inch"
    static let emptyWeight                      : String = "Please enter weight"
    static let emptyWingSpan                    : String = "Please enter wingspan"
    static let emptyWingSpanFeet                : String = "Please enter wingspan feet"
    static let emptyWingSpanInch                : String = "Please enter wingspan inch"
    static let emptyHandMeasurement             : String = "Please enter hand measurement"
    static let emptyEducationLevel              : String = "Please select current education level"
    static let emptyGrade                       : String = "Please enter grade point"
    static let emptyDominantHand                : String = "Please select dominant hand"
    static let emptyDominantFoot                : String = "Please select dominant foot"
    
    static let emptySchoolPlayingLevel          : String = "Please select school playing level"
    static let emptyCompetitionLevel            : String = "Please select competition level"
    static let emptyClub                        : String = "Please enter Rec/Club/Travel Level"
    
    static let emptySports                      : String = "Please select atleast one sport."
    static let emptyColleges                    : String = "Please select atleast one college."
    static let emptySportsPosition              : String = "Please select selected sport's position"

    static let emptyBankName                    : String = "Please enter bank name."
    static let emptyAccountHolderName           : String = "Please enter account holder name."
    static let emptyAccountNumber               : String = "Please enter account number."
    static let emptyIfscCode                    : String = "Please enter IFSC code."
    static let cancelcoupon                     : String = "Do you want to remove this coupon code?"
    static let imageSizeLimit                   : String = "File size exceed 2MB"
    static let emptyImage                       : String = "Please choose a image"
    static let emptyName                        : String = "Please enter your name."
    static let emptyGender                      : String = "Please select gender"
    static let emptyDOB                         : String = "Please choose date of birth"
    static let emptyYear                        : String = "Please choose year of birth"
    static let emptyGraduationYear              : String = "Please choose year of graduation"
    static let emptyMonth                       : String = "Please choose month of birth"
    static let emptyDay                         : String = "Please choose day of birth"
    static let emptyAbout                       : String = "Please tell us about this photo."
    static let emptyProductItem                 : String = "Please choose a product item"
    static let exceedQuantityTryAndBuy          : String = "You can add single quantity at a time for try and buy"
    static let emptyMessage                     : String = "Please enter message for report."
    static let childDeleteMessage               : String = "Are you sure you want to delete this child?"
    static let emptyAge                         : String = "Please enter paitent age."
    static let kworkinprogress                  : String = "Work in progress."
    static let emptySchool                      : String = "Please enter school name."
    static let emptyPreference                  : String = "Please select contact preference"
    static let emptyBioText                     : String = "Please enter you bio"
    static let emptyBioLink                     : String = "Please enter you bio link"
    
    static let emptyRelationship                : String = "Please select relationship"
    static let emptyPhoneType                   : String = "Choose primary phone type"
    
    static let emptyWorkouts                    : String = "Please select atleast one Workout."
    
    static let emptyEventCategory               : String = "Please select event category"
    static let emptyEventName                   : String = "Please enter event name"
    static let emptyEventLocation               : String = "Please enter event location"
    static let emptyStartDate                   : String = "Please enter start date"
    static let emptyEndDate                     : String = "Please enter end date"
    static let emptyEventNote                   : String = "Please enter event note"
    static let emptyEventDetails                : String = "Please enter event details"
    static let emptyEventDetailsLink            : String = "Please enter url of event"
    static let emptyStatus                      : String = "Please enter event status"
    
    
    static let emptyRecodedDate                 : String = "Please select recoded date"
    static let emptyDescription                 : String = "Please enter description"
    static let emptyVideoLink                   : String = "Please enter video link"
    static let emptyWorkoutCategory             : String = "Please select workout category"
    static let emptyWorkoutType                 : String = "Please select workout type"
    static let emptyCoachinLevel                : String = "Please select coachin level"
    static let emptySportLevel                  : String = "Please select sport level"
    static let emptyTeamName                    : String = "Please enter team name"
    static let emptyGroupName                   : String = "Please enter group name"
    static let emptyUnitField                   : String = "Please enter unit"
    static let emptyComparisonArray             : String = "Select connetions to compare"
    static let emptyComparionName               : String = "Please enter comparison name"
    

}

struct Device {
    // iDevice detection code
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0

    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )

    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
}

struct WorkoutMeasurement {
    static let reps             : String = "Enter the number of reps completed | Enter the amount of weight used"
    static let time             : String = "Enter the time it took to complete the workout"
    static let distance         : String = "Enter the distance they jumped"
    static let height           : String = "Enter the height of the box on which they jumped"
    static let distance_weight  : String = "Enter distance travelled | Enter the amount of weight used"
    static let speed            : String = "Enter MPH"
    static let percentage       : String = "Enter Percentage"
}

//typealias CurrentLocationFetchCompletionBlock = (_ isCompleted:Bool, _ location: [CLLocation]) -> Void

typealias ImageCompletionBlock = (_ isCompleted:Bool, _ userInfo: [String : Any]) -> Void
typealias ImageDeletionBlock = (_ isDeleted:Bool) -> Void
typealias CurrentLocationFetchCompletionBlock = (_ isCompleted:Bool, _ location: [CLLocation]) -> Void
typealias TextFieldCompletionBlock = (  _ textField: UITextField,  _ texFieldDelegType: TextFieldDelegateType) -> Bool?
typealias TextFieldShouldChangeCompletionBlock = (  _ textField: UITextField,  _ text: String) -> Bool?

typealias TextViewShouldChangeCompletionBlock = (  _ textView: UITextView,  _ text: String) -> Bool?
typealias TextViewCompletionBlock = (  _ textView: UITextView,  _ texFieldDelegType: TextViewDelegateType) -> Bool?
typealias ProfileEditSaveCompletionBlock = (_ isTickbutton:UIButton) -> Bool?
typealias NetworkAvailibilityCompletionBlock = (_ isNetworkAvailable:Bool, _ type: String) -> Void

typealias DidSelectCollectionCellCompletionBlock = (_ selectedIndex:Int, _ data: Any) -> Void

typealias DidSelectTableViewCellCompletionBlock = (_ selectedIndex:Int, _ data: Any) -> Void
typealias ButtonCompletionBlock = (_ selectedIndex:Int, _ data: Any) -> Void


//typealias GestureCompletionBlock = (  _ indexpath: IndexPath, _ cell: MyServiceEditUploadMenuImageCollectionViewCell, _ gestureType: GestureType ) -> Void


enum GestureType {
    case longPressGesture
    case tapGesture
}

enum TextFieldDelegateType {
    case textFieldDidBeginEditing
    case textFieldShouldChangeCharactersIn
    case textFieldShouldReturn
    case textFieldDidEndEditing
}

enum TextViewDelegateType {
    case didChange
}

enum ApiStatus: Int {
    case falure = 0
    case success = 1
    case tokenExpire = 2
    case missingParameter = 404
}

public enum ListType: String {
    public typealias RawValue = String
    
    case workoutcategory = "workoutcategory"
    case workoutcategorydetails = "workoutcategorydetails"
    case eventlist = "eventlist"
    case eventfieldlist = "eventfieldlist"
    case workoutcategorytips = "workoutcategorytips"
    case gamehighlightslist = "gamehighlightslist"
    case gamehighlightsadddatalist = "gamehighlightsadddatalist"
    case videoevidencelist = "videoevidencelist"
    case videoevidenceadddatalist = "videoevidenceadddatalist"
    case teamingupgrouplist = "teamingupgrouplist"
    case teamingupgroupdetailslist = "teamingupgroupdetailslist"
    case teamingupgroupadddatalist = "teamingupgroupadddatalist"
    case teamingupgroupinvitelist = "teamingupgroupinvitelist"
    case teamingupgroupinviteUserlist = "teamingupgroupinviteUserlist"
    case allAthleteFolowerUserlist = "allAthleteFolowerUserlist"
    case allCoachFolowerUserlist = "allCoachFolowerUserlist"
    case workoutupdatefields = "workoutupdatefields"
    case connectionList = "connectionList"
    case compareTagList = "compareTagList"
    case compareAthelteList = "compareAthelteList"
    case compreGroupList = "compreGroupList"
    case compareSavedItemList = "compareSavedItemList"
    case recomendationList = "recomendationList"
    case recomendationorderList = "recomendationorderList"
    case recomendationuserList = "recomendationuserList"
    case recomendationtagList = "recomendationtagList"
    case profileTabList = "profileTabList"
    case eventTaglist = "eventTaglist"
    
}

public enum LoginType {
    case athlete
    case coach
}

public enum AccountType {
    case athlete
    case coach
}

enum RootControllerType {
    case RootControllerTypeInitial
    case RootControllerTypeLogin
    case RootControllerTypeHome
    case RootControllerTypeSchedule
    case RootControllerTypeAppointments
    case RootControllerTypeTask
    case RootControllerTypeLeads
    case RootControllerTypeCustomer
    case RootControllerTypeUnit
    case RootControllerTypeWorkOrder
    case RootControllerTypeParts
    case RootControllerTypeDocuments
    case RootControllerTypeReports
    case RootControllerTypeMyAccounts
    case RootControllerTypeNotifications
    
}

public enum RegistrationSteps {
   case basic
   case address
   case physical
   case sports
   case position
   case college
   case subscription
   case compete
   case parent
}

public enum PickerType : String {
    public typealias RawValue = String
    
    case city = "city"
    case state = "state"
    case county = "county"
    case filtercollege = "filtercollege"
    case schoolplayinglevel = "schoolplayinglevel"
    case educationlevel = "educationlevel"
    case club = "club"
    case weight = "weight"
    case year = "year"
    case month = "month"
    case day = "day"
    case preference = "preference"
    case sportposition = "sportposition"
    case sport = "sport"
    case colleges = "colleges"
    case relationship = "relationship"
    case competitivelevel = "competitivelevel"
    case zip = "zip"
    case status = "status"
    case eventcategory = "eventcategory"
    case workoutcategory = "workoutcategory"
    case workouttype = "workouttype"
    case coachinlevel = "coachinlevel"
    case sportlevel = "sportlevel"
    case dominantlevel = "dominantlevel"
    case recomendationidtype = "recomendationidtype"
    case multiplesport = "multiplesport"
    
}

public enum ModelType : String {
    public typealias RawValue = String
    
    case basic = "basic"
    case basiccoach = "basiccoach"
    case address = "address"
    case physical = "physical"
    case other = "other"
    case parent = "parent"
    case event = "event"
    case game = "game"
    case video = "video"
    case compete = "compete"
    case group = "group"
    case workoutupdate = "workoutupdate"
    case college = "college"
    case sports = "sports"
}


