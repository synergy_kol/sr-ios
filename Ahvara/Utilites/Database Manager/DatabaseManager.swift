//
//  DatabaseManager.swift
//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.
//

import UIKit
import MagicalRecord

class DatabaseManager: NSObject {

    class func saveDB() {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
    }
 /*   class func clearAllLocalData() {
        UserDetails.mr_truncateAll()
        self.saveDB()
    }
    
    //MARK:- UserProfile
    class func currentUserItem() -> (UserDetails?) {
        return UserDetails.mr_findFirst()
    }

    class func updateUserInfoForKey(strKey: String, strValue: String) {
        var userData = UserDetails.mr_findFirst()
        if userData == nil {
            userData = UserDetails.mr_createEntity()
        }
        userData?.setValue(strValue, forKey: strKey)
        self.saveDB()
    }
    
    class func saveUserDetailsData(dict: [String:Any], token : String) -> Void {
                   
            if let userDetails = self.currentUserItem() {
            
                userDetails.accesstoken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: token , defaultValue: userDetails.accesstoken ?? "")
                
                userDetails.devicetoken = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict["device_token"] , defaultValue: userDetails.devicetoken ?? "")
                
                userDetails.loginstatus = Common.sharedInstance.getStringValueFromAPIValue(apiValue: 1 , defaultValue: userDetails.loginstatus ?? "0")
                
                userDetails.graduationyear = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["graduation_year"] , defaultValue: (userDetails.graduationyear)!)
                
                userDetails.gender = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["gender"] , defaultValue: (userDetails.gender)!)
                
                userDetails.mobile_verified_at = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["mobile_verified_at"] , defaultValue: (userDetails.mobile_verified_at)!)
                
                userDetails.birthyear = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["year"] , defaultValue: (userDetails.birthyear)!)
                
                userDetails.created_at = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["created_at"] , defaultValue: (userDetails.created_at)!)
                
                userDetails.birthmonth = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["month"] , defaultValue: (userDetails.birthmonth)!)
                
                userDetails.username = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["username"] , defaultValue: (userDetails.username)!)
                
                
                userDetails.userid = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["id"] , defaultValue: (userDetails.userid)!)
                
                
                userDetails.date_of_year = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["date_of_year"] , defaultValue: (userDetails.date_of_year)!)
                
                
                userDetails.profile_image = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["profile_image"] , defaultValue: (userDetails.profile_image)!)
                
                
                userDetails.birthday = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["day"] , defaultValue: (userDetails.birthday)!)
                
                
                userDetails.email = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["email"] , defaultValue: (userDetails.email)!)
                
                
                userDetails.role_id = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["role_id"] , defaultValue: (userDetails.role_id)!)
                
                
                userDetails.email_verified_at = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["email_verified_at"] , defaultValue: (userDetails.email_verified_at)!)
                
                
                userDetails.profile_type = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["profile_type"] , defaultValue: (userDetails.profile_type)!)
                
                
                userDetails.mobile = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["mobile"] , defaultValue: (userDetails.mobile)!)
                
                
                userDetails.status = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["status"] , defaultValue: (userDetails.status)!)
                
                
                userDetails.updated_at = Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["updated_at"] , defaultValue: (userDetails.updated_at)!)
                
            } else {
                let userDetails = UserDetails.mr_createEntity()
                let userDetailsDict = ["accesstoken" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: token , defaultValue: ""),
                                       "gender" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["gender"] , defaultValue: ""),
                                       "mobile_verified_at" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["mobile_verified_at"] , defaultValue: ""),
                                       "devicetoken" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["device_token"] , defaultValue: ""),
                                       "loginstatus" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: 1, defaultValue: "1"),
                                       "graduationyear" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["graduation_year"] , defaultValue: ""),
                                       "birthyear" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["year"] , defaultValue: ""),
                                       "created_at" :  Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["created_at"] , defaultValue: ""),
                                       "birthmonth" :  Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["month"] , defaultValue: ""),
                                       "username" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["username"] , defaultValue: ""),
                                       "userid" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["id"] , defaultValue: ""),
                                       "date_of_year" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["date_of_year"] , defaultValue: ""),
                                       "profile_image" :  Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["profile_image"] , defaultValue: ""),
                                       "birthday" :  Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["day"] , defaultValue: ""),
                                       "email" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["email"] , defaultValue: ""),
                                       "role_id" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["role_id"] , defaultValue: ""),
                                       "email_verified_at" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["email_verified_at"] , defaultValue: ""),
                                       "profile_type" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["profile_type"] , defaultValue: ""),
                                       "mobile" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["mobile"] , defaultValue: ""),
                                       "status" : Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["status"] , defaultValue: ""),
                                       "updated_at" :  Common.sharedInstance.getStringValueFromAPIValue(apiValue: dict ["updated_at"] , defaultValue: "")
                                       
                    ] as [String : Any]
                userDetails?.mr_importValuesForKeys(with: userDetailsDict)
            }
            self.saveDB()
    }
    
    class func deleteUserDetails() -> Void {
        UserDetails.mr_truncateAll()
        self.saveDB()
    }*/

}
