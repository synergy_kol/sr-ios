//
//  TabBarViewController.swift
//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.
//

import UIKit

class TabBarViewController: UIViewController {
    
    
    @IBOutlet var viewForAthelte: UIView!
    @IBOutlet var viewForCoach: UIView!
    
    
    @IBOutlet var btnHomeForAthlete: UIButton!
    @IBOutlet var btnWorkoutForAthlete: UIButton!
    @IBOutlet var btnCompareForAthlete: UIButton!
    @IBOutlet var btnProfileForAthlete: UIButton!
    @IBOutlet var btnSettingForAthlete: UIButton!
    
    
    @IBOutlet var btnHomeForCoach: UIButton!
    @IBOutlet var btnGroupForCoach: UIButton!
    @IBOutlet var btnLookupForCoach: UIButton!
    @IBOutlet var btnRecomendationCoach: UIButton!
    @IBOutlet var btnProfileForCoach: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        /*if(appDelegate.loginType == LoginType.coach) {
            self.viewForAthelte.isHidden    = true
            self.viewForCoach.isHidden = false
            self.updateTabBarForCoach(tabIndex: 0)
        } else {
            self.viewForAthelte.isHidden    = false
            self.viewForCoach.isHidden = true
            self.updateTabBarForAthelete(tabIndex: 0)
        }*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated) // this line need
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Helper Method
    
    /*Home*/
    func selectHomeTab() {
        /*if(appDelegate.loginType == LoginType.coach) {
            self.btnHomeForCoach.isSelected = true
        } else {
            self.btnHomeForAthlete.isSelected = true
        }*/
    }
    func deselectHomeTab() {
       /* if(appDelegate.loginType == LoginType.coach) {
            self.btnHomeForCoach.isSelected = false
        } else {
            self.btnHomeForAthlete.isSelected = false
        }*/
    }
    
    /*Workout*/
    func selectWorkoutTab(){
        self.btnWorkoutForAthlete.isSelected = true
    }
    
    func deselectWorkoutTab() {
        self.btnWorkoutForAthlete.isSelected = false
    }
    
    /*Compare*/
    func selectCompareTab(){
        self.btnCompareForAthlete.isSelected = true
    }
    
    func deselectCompareTab() {
        self.btnCompareForAthlete.isSelected = false
    }
    
    /*Profile*/
    func selectProfileTab() {
       /* if(appDelegate.loginType == LoginType.coach) {
            self.btnProfileForCoach.isSelected = true
        } else {
            self.btnProfileForAthlete.isSelected = true
        }*/
    }
    func deselectProfileTab() {
       /* if(appDelegate.loginType == LoginType.coach) {
            self.btnProfileForCoach.isSelected = false
        } else {
            self.btnProfileForAthlete.isSelected = false
        }*/
    }
    
    /*Setting*/
    func selectSettingTab(){
        self.btnSettingForAthlete.isSelected = true
    }
    
    func deselectSettingTab() {
        self.btnSettingForAthlete.isSelected = false
    }
    
    /*Group*/
    func selectGroupTab(){
        self.btnGroupForCoach.isSelected = true
    }
    
    func deselectGroupTab() {
        self.btnGroupForCoach.isSelected = false
    }
    
    /*Look up*/
    func selectLookupTab(){
        self.btnLookupForCoach.isSelected = true
    }
    
    func deselectLookupTab() {
        self.btnLookupForCoach.isSelected = false
    }
    
    /*Recomendation*/
    func selectRecomendationTab(){
        self.btnRecomendationCoach.isSelected = true
    }
    
    func deselectRecomendationTab() {
        self.btnRecomendationCoach.isSelected = false
    }
    
    
    
    
    func updateTabBarForAthelete(tabIndex:Int) {
        switch tabIndex {
        case 0:
            self.selectHomeTab()
            self.deselectWorkoutTab()
            self.deselectCompareTab()
            self.deselectProfileTab()
            self.deselectSettingTab()
        case 1:
            
            self.deselectHomeTab()
            self.selectWorkoutTab()
            self.deselectCompareTab()
            self.deselectProfileTab()
            self.deselectSettingTab()
            
        case 2:
            
            self.deselectHomeTab()
            self.deselectWorkoutTab()
            self.selectCompareTab()
            self.deselectProfileTab()
            self.deselectSettingTab()
        case 3:
            self.deselectHomeTab()
            self.deselectWorkoutTab()
            self.deselectCompareTab()
            self.selectProfileTab()
            self.deselectSettingTab()
        case 4:
            self.deselectHomeTab()
            self.deselectWorkoutTab()
            self.deselectCompareTab()
            self.deselectProfileTab()
            self.selectSettingTab()
        default:
            self.selectHomeTab()
            self.deselectWorkoutTab()
            self.deselectCompareTab()
            self.deselectProfileTab()
            self.deselectSettingTab()
        }
    }
    
    func updateTabBarForCoach(tabIndex:Int) {
        switch tabIndex {
        case 0:
            self.selectHomeTab()
            self.deselectGroupTab()
            self.deselectLookupTab()
            self.deselectRecomendationTab()
            self.deselectProfileTab()
        case 1:
            self.deselectHomeTab()
            self.selectGroupTab()
            self.deselectLookupTab()
            self.deselectRecomendationTab()
            self.deselectProfileTab()
        case 2:
            self.deselectHomeTab()
            self.deselectGroupTab()
            self.selectLookupTab()
            self.deselectRecomendationTab()
            self.deselectProfileTab()
        case 3:
            self.deselectHomeTab()
            self.deselectGroupTab()
            self.deselectLookupTab()
            self.selectRecomendationTab()
            self.deselectProfileTab()
        case 4:
            self.deselectHomeTab()
            self.deselectGroupTab()
            self.deselectLookupTab()
            self.deselectRecomendationTab()
            self.selectProfileTab()
        default:
            self.selectHomeTab()
            self.deselectGroupTab()
            self.deselectLookupTab()
            self.deselectRecomendationTab()
            self.deselectProfileTab()
        }
    }
}
