//
//  CustomToastView.swift
//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.
//

import UIKit

class CustomToastView: UIView {
    @IBOutlet weak var lbl_toastMessage: UILabel!
    
    @IBOutlet weak var btn_toastMessage: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    class func instanceFromNib() -> CustomToastView {
        return UINib(nibName: "CustomToastView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomToastView
    }

}
