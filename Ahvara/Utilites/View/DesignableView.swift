//
//  DesignableView.swift
//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.
//

import UIKit

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableStackView: UIStackView {

}

@IBDesignable
class DesignableTableView: UITableView {

}

@IBDesignable
class DesignableButton: UIButton {
    
}

@IBDesignable
class DesignableLabel: UILabel {
}

@IBDesignable
class DesignableImageView: UIImageView {

}

@IBDesignable
class DesignableTextField: UITextField {

}

extension UIView {

    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    @IBInspectable
    var masksToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }

    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }

    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }

    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }

    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }

    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}

extension UITextField {
    @IBInspectable var setImage: UIImage {
                get{
                    return UIImage(named: "")!
                }
                set{
                    let imageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                    imageView.image = newValue
                    self.rightView = imageView
                    self.rightViewMode = UITextField.ViewMode.always
            }
        }
    
    
    @IBInspectable var paddingLeftCustom: CGFloat {
            get {
                return leftView!.frame.size.width
            }
            set {
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
                leftView = paddingView
                leftViewMode = .always
            }
        }

        @IBInspectable var paddingRightCustom: CGFloat {
            get {
                return rightView!.frame.size.width
            }
            set {
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
                rightView = paddingView
                rightViewMode = .always
            }
        }    
}

extension UITextView:NSTextStorageDelegate {

    @IBInspectable var topPadding: CGFloat {
            get {
                return contentInset.top
            }
            set {
                self.contentInset = UIEdgeInsets(top: newValue,
                                                 left: self.contentInset.left,
                                                 bottom: self.contentInset.bottom,
                                                 right: self.contentInset.right)
            }
        }

        @IBInspectable var bottomPadding: CGFloat {
            get {
                return contentInset.bottom
            }
            set {
                self.contentInset = UIEdgeInsets(top: self.contentInset.top,
                                                 left: self.contentInset.left,
                                                 bottom: newValue,
                                                 right: self.contentInset.right)
            }
        }

        @IBInspectable var leftPadding: CGFloat {
            get {
                return contentInset.left
            }
            set {
                self.contentInset = UIEdgeInsets(top: self.contentInset.top,
                                                 left: newValue,
                                                 bottom: self.contentInset.bottom,
                                                 right: self.contentInset.right)
            }
        }

        @IBInspectable var rightPadding: CGFloat {
            get {
                return contentInset.right
            }
            set {
                self.contentInset = UIEdgeInsets(top: self.contentInset.top,
                                                 left: self.contentInset.left,
                                                 bottom: self.contentInset.bottom,
                                                 right: newValue)
            }
        }
    
    private class PlaceholderLabel: UILabel { }

        private var placeholderLabel: PlaceholderLabel {
            if let label = subviews.compactMap( { $0 as? PlaceholderLabel }).first {
                return label
            } else {
                let label = PlaceholderLabel(frame: .zero)
                label.font = font
                addSubview(label)
                return label
            }
        }

        @IBInspectable
        var placeholder: String {
            get {
                return subviews.compactMap( { $0 as? PlaceholderLabel }).first?.text ?? ""
            }
            set {
                let placeholderLabel = self.placeholderLabel
                placeholderLabel.text = newValue
                placeholderLabel.textColor = .white
                placeholderLabel.numberOfLines = 0
                let width = frame.width - textContainer.lineFragmentPadding * 2
                let size = placeholderLabel.sizeThatFits(CGSize(width: width, height: .greatestFiniteMagnitude))
                placeholderLabel.frame.size.height = size.height
                placeholderLabel.frame.size.width = width
                placeholderLabel.frame.origin = CGPoint(x: textContainer.lineFragmentPadding, y: textContainerInset.top)

                textStorage.delegate = self
            }
        }
    
    public func textStorage(_ textStorage: NSTextStorage, didProcessEditing editedMask: NSTextStorage.EditActions, range editedRange: NSRange, changeInLength delta: Int) {
           if editedMask.contains(.editedCharacters) {
               placeholderLabel.isHidden = !text.isEmpty
           }
       }
}



