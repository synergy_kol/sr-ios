//
//  LocationManager.swift
//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.
//

import UIKit
import GoogleMaps
import Alamofire
class LocationManager: NSObject {
    
    static let sharedInstance = LocationManager()
    var locationManager = CLLocationManager()
    var currentLocations = [CLLocation]()
    var completionBlock: CurrentLocationFetchCompletionBlock?
    
  
    
    @available(iOS 14.0, *)
    public func getcurrentLocation(completionhandler:@escaping CurrentLocationFetchCompletionBlock) {
        
        self.completionBlock = completionhandler
        locationManager.delegate = self
        //let window = UIApplication.shared.keyWindow
        if #available(iOS 14.0, *) {
            if locationManager.authorizationStatus == .denied {
                //self.commonAlert(title: "", msg: "To show your position on the map, you have to enable location services and authorize the app", curView: (window?.rootViewController)!)
            }
            if locationManager.authorizationStatus == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
        } else {
            if CLLocationManager.authorizationStatus() == .denied {
                //self.commonAlert(title: "", msg: "To show your position on the map, you have to enable location services and authorize the app", curView: (window?.rootViewController)!)
            }
            if CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
        }
      
        
        //locationManager.desiredAccuracy    = kCLLocationAccuracyBest
        //locationManager.distanceFilter     = 100
        locationManager.startUpdatingLocation()
    }
    
    func getPlaceAddressFromLatLng(latitude: Double, longitude: Double, completion: @escaping (_ placemark: [CLPlacemark]?, _ error: Error?) -> Void)  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { placemark, error in
            guard let placemark = placemark, error == nil else {
                completion(nil, error)
                return
            }
            completion(placemark, nil)
        }
    }
    
    func getAddressFromLatLongUsingGeocoder(latitude: Double, longitude : Double,completion: @escaping (_ result: [AnyObject]?, _ error: Error?) -> Void) {
     
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=" + Credential.GoogleApiKey
       
        AF.request(url).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let responseJson = value as! NSDictionary
                
                if let results = responseJson.object(forKey: "results")! as? [NSDictionary] {
                    if results.count > 0 {
                        completion(results,nil)
                    /*    if let addressComponents = results[0]["address_components"]! as? [NSDictionary] {
                           let address = results[0]["formatted_address"] as? String
                            for component in addressComponents {
                                if let temp = component.object(forKey: "types") as? [String] {
                                    
                                }
                            }
                        }*/
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}


extension LocationManager : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
            
        @unknown default:
            print("Location status is OK.")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        self.currentLocations = [location]
        self.completionBlock!(true,[location])
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // print("Did location updates is called but failed getting location \(error)")
        self.completionBlock!(false,[])
    }
}

    //MARK: *************************Places API GOOGLE***************************************
