//
//  Common.swift
//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.
//

import UIKit
import ActionKit
//import GoogleMaps
import CropViewController
import Photos
import AVFoundation
import MobileCoreServices
import KYDrawerController

class Common: NSObject{

    var zipCodeArray : [[String:Any]] = [[String:Any]]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    static let sharedInstance = Common()
    
    var completionBlock: ImageCompletionBlock?
    var deletionBlock: ImageDeletionBlock?
    
    var tabBarController : UITabBarController!
    //var menuTabBarController : UITabBarController!
    var tabBarVcObj : TabBarViewController!
    var tabBarView : UIView!
    var selectedAccountType: AccountType?
    var userId: Int?
    var userData: [String:AnyObject]?
    var userToken: String?
    
        
    
    
    /*class var isUserLoggedIn : Bool {
        guard let currentUserItem  = DatabaseManager.currentUserItem() else { return false }
        guard let status = currentUserItem.loginstatus else { return false }
        
        if(status == "1") {
            return true
        } else {
            return false
        }
    }*/
    
    public func attributedString(Font1: UIFont!, Font2: UIFont!, from string: String, nonBoldRange: NSRange?, Colour1: UIColor, Colour2: UIColor) -> NSAttributedString {
        //let fontSize = UIFont.systemFontSize
        let attrs = [
            NSAttributedString.Key.font : Font1,
            NSAttributedString.Key.foregroundColor: Colour1
        ]
        let nonBoldAttribute = [
            NSAttributedString.Key.font : Font2,
            NSAttributedString.Key.foregroundColor: Colour2
        ]
        let attrStr = NSMutableAttributedString(string: string, attributes: attrs as [NSAttributedString.Key : Any])
        if let range = nonBoldRange {
            attrStr.setAttributes(nonBoldAttribute as [NSAttributedString.Key : Any], range: range)
        }
        return attrStr
    }
     // MARK: - TimeZone  ▶︎
  
    class func getCurrentTimeZone() -> String{
        
        return TimeZone.current.identifier
        
    }
    
    public func getChangeDateFormat(date: String?, inputFormat: String?, requiredFormat: String?) -> String
    {
        //2021-10-29 00:00:00 yyyy-MM-dd hh:mm:ss
        //Get the current time stamp
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = inputFormat
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = requiredFormat
        dateFormatterPrint.timeZone = TimeZone.current
        
        
            let date: NSDate? = dateFormatterGet.date(from: date ?? "") as NSDate?
        
        if(date != nil) {
            return(dateFormatterPrint.string(from: date! as Date))
        } else {
            return ""
        }
    }
    
    // MARK: - GetVideoID From Video URL  ▶︎
    public func getVideoID(from urlString: String) -> String? {
        guard let url = urlString.removingPercentEncoding else { return nil }
        do {
            let regex = try NSRegularExpression.init(pattern: "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)", options: .caseInsensitive)
            let range = NSRange(location: 0, length: url.count)
            if let matchRange = regex.firstMatch(in: url, options: .reportCompletion, range: range)?.range {
                let matchLength = (matchRange.lowerBound + matchRange.length) - 1
                if range.contains(matchRange.lowerBound) &&
                    range.contains(matchLength) {
                    let start = url.index(url.startIndex, offsetBy: matchRange.lowerBound)
                    let end = url.index(url.startIndex, offsetBy: matchLength)
                    return String(url[start...end])
                }
            }
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
    //MARK: - Resize Image ------
    class func resizeImage(image: UIImage, width : Float) -> Data {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        var imgRatio: Float = actualWidth / actualHeight
        let maxHeight: Float = width * imgRatio
        let maxWidth: Float = width
        let maxRatio: Float = maxWidth / maxHeight
        //let compressionQuality: Float = 0.5
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x:0.0,y: 0.0, width:CGFloat(actualWidth), height:CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img!.jpegData(compressionQuality: 1.0)
        UIGraphicsEndImageContext()
        return imageData!
    }
    
    // MARK: - StringFromDate  ▶︎
    
    public func getCurrentDateTime(date: String) -> String
    {
        //Get the current time stamp
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "d MMM yy"
        
        let date: NSDate? = dateFormatterGet.date(from: date) as NSDate?
        return(dateFormatterPrint.string(from: date! as Date))
    }
    // MARK: - Get Current Time stamp  ▶︎
    class func getCurrentTimeStamp() -> Int{
        //Get the current time stamp
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
    
   class func convertMilliToDate(milliseconds: Int64) -> Date {
        return Date(timeIntervalSince1970: (TimeInterval(milliseconds) / 1000))
    }
    
    class func getFiveYearsPiriorFromCurrentYear() -> Int {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        let dateStr = formatter.string(from: Date())
        let year = (Int(dateStr)! - 5)
        return year
     }
    
    class func getCurrentYear() -> Int {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        let dateStr = formatter.string(from: Date())
        let year = Int(dateStr)!
        return year
     }
    
    //MARK:- Navigation control
    class func getVCWithId(identifier:String, storyBoardName:String) -> UIViewController
    {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: identifier)
        
        return vc
    }
    // MARK:- Convert To Json string
    public func convertIntoJSONString(arrayObject: [Any]) -> String? {
        
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
            
        } catch let error as NSError {
            print("Array convertIntoJSON - \(error.description)")
        }
        return nil
    }
    
    //MARK:- Convert string from any value
    public func getStringValueFromAPIValue(apiValue: Any?, defaultValue: String) -> String {
        
        var valueString = String(describing: apiValue ?? defaultValue)
        
        if ((valueString == "(NULL)") || (valueString == "<NULL>") || (valueString == "<null>") || (valueString == "(null)") || (valueString == "")) {
            valueString = defaultValue
        }
        
        return valueString
    }
    
    // MARK:- Change Root Controller
    class func changeRootController(rootControllerType: RootControllerType) -> Void {
        
        let scenes = UIApplication.shared.connectedScenes
        let windowScene = scenes.first as? UIWindowScene
        let window = windowScene?.windows.first

        
        if (rootControllerType == .RootControllerTypeLogin) {
            
            let storyboard = UIStoryboard.storyboardMain()
            let controller = storyboard.instantiateViewController(withIdentifier: "AInitialViewController") as! AInitialViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()

            
        } else if (rootControllerType == .RootControllerTypeHome) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "AHomeViewController") as! AHomeViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        } else if (rootControllerType == .RootControllerTypeSchedule) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "AScheduleViewController") as! AScheduleViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        }  else if (rootControllerType == .RootControllerTypeAppointments) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "AAppointmentViewController") as! AAppointmentViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        } else if (rootControllerType == .RootControllerTypeTask) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "ATaskViewController") as! ATaskViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        }else if (rootControllerType == .RootControllerTypeLeads) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "ALeadsViewController") as! ALeadsViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        }else if (rootControllerType == .RootControllerTypeCustomer) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "ACustomerListViewController") as! ACustomerListViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        }else if (rootControllerType == .RootControllerTypeUnit) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "AUnitViewController") as! AUnitViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        } else if (rootControllerType == .RootControllerTypeWorkOrder) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "AWorkOrderViewController") as! AWorkOrderViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        }else if (rootControllerType == .RootControllerTypeDocuments) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "ADocumentsViewController") as! ADocumentsViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        }else if (rootControllerType == .RootControllerTypeReports) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "AReportViewController") as! AReportViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        }else if (rootControllerType == .RootControllerTypeMyAccounts) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "AMyAccountViewController") as! AMyAccountViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        }else if (rootControllerType == .RootControllerTypeNotifications) {
            
            let storyboard = UIStoryboard.storyboardHome()
            let controller = storyboard.instantiateViewController(withIdentifier: "ANotificationViewController") as! ANotificationViewController
            let navController = UINavigationController(rootViewController: controller)
            navController.isNavigationBarHidden = true
            window?.rootViewController = navController
            window?.makeKeyAndVisible()
            
        }
        
    }
    
    
    // MARK:- Tab Bar Setup
    class func setupTabBarController() {
                
        self.sharedInstance.tabBarController = UITabBarController()
        self.sharedInstance.tabBarController.tabBar.isHidden = true
            
        
        /* Athlete Home*/
       /*
        let homeAthleteVC = UIStoryboard.storyboardHome().instantiateViewController(withIdentifier:"PAthleteHomeViewController") as! PAthleteHomeViewController
        let homeNavVC = UINavigationController(rootViewController: homeAthleteVC)
        homeNavVC.isNavigationBarHidden = true
        
        /* Workout*/
        let workoutVC = UIStoryboard.storyboardHome().instantiateViewController(withIdentifier:"PWorkoutCategoryViewController") as! PWorkoutCategoryViewController
        let workoutNavVC = UINavigationController(rootViewController: workoutVC)
        workoutNavVC.isNavigationBarHidden = true
        
        /* Home Coach*/
        let homeCoachVC = UIStoryboard.storyboardHome().instantiateViewController(withIdentifier:"PCoachHomeViewController") as! PCoachHomeViewController
        let homeCoachNavVC = UINavigationController(rootViewController: homeCoachVC)
        homeCoachNavVC.isNavigationBarHidden = true
        
        /* Compare*/
        let compareVC = UIStoryboard.storyboardHome().instantiateViewController(withIdentifier:"PCompareListViewController") as! PCompareListViewController
        let compareNavVC = UINavigationController(rootViewController: compareVC)
        compareNavVC.isNavigationBarHidden = true
        
        /* Profile*/
        let profileVC = UIStoryboard.storyboardHome().instantiateViewController(withIdentifier:"PProfileDetailsViewController") as! PProfileDetailsViewController
        profileVC.isOwnProfile = true
        
        let userid = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.userid, defaultValue: "")
        let userName = Common.sharedInstance.getStringValueFromAPIValue(apiValue: DatabaseManager.currentUserItem()?.username, defaultValue: "")
                
        profileVC.userName = userName
        profileVC.userid = userid
        
        let profileNavVC = UINavigationController(rootViewController: profileVC)
        
        profileNavVC.isNavigationBarHidden = true
        
        
        
        /*/* Profile */
        let profileVC = UIStoryboard.storyboardMain().instantiateViewController(withIdentifier: ProfileViewController.string) as! ProfileViewController
        let profileNavVC = UINavigationController(rootViewController: profileVC)
        profileNavVC.isNavigationBarHidden = true
        
        
        /* Settings */
        let settingVC = UIStoryboard.storyboardMain().instantiateViewController(withIdentifier: SettingViewController.string) as! SettingViewController
        let settingNavVC = UINavigationController(rootViewController: settingVC)
        settingNavVC.isNavigationBarHidden = true
        
        /* Earnings */
        let earningVC = UIStoryboard.storyboardMain().instantiateViewController(withIdentifier: EarningViewController.string) as! EarningViewController
        let earningNavVC = UINavigationController(rootViewController: earningVC)
        earningNavVC.isNavigationBarHidden = true
        */
        
        self.sharedInstance.tabBarController.viewControllers = (self.sharedInstance.appDelegate.loginType == LoginType.athlete) ? [homeAthleteVC, workoutVC, compareVC, profileVC] : [homeCoachVC]
                
        let tabBar = UIStoryboard.storyboardHome().instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.sharedInstance.tabBarController.view.addSubview(tabBar.view)
        
        let tabBarHeight : CGFloat = 80.0
        tabBar.view.autoPinEdge(toSuperviewEdge: .leading, withInset: 0.0)
        tabBar.view.autoPinEdge(toSuperviewEdge: .bottom, withInset: 0.0)
        tabBar.view.autoPinEdge(toSuperviewEdge: .trailing, withInset: 0.0)
        tabBar.view.autoSetDimension(.height, toSize: tabBarHeight)
        
        self.sharedInstance.tabBarView = tabBar.view
        
        if(self.sharedInstance.appDelegate.loginType == LoginType.athlete) {
            
            tabBar.btnHomeForAthlete.removeControlEvent(.touchUpInside)
            tabBar.btnHomeForAthlete.addControlEvent(.touchUpInside) {
                
            self.sharedInstance.tabBarController.selectedIndex = 0
            tabBar.updateTabBarForAthelete(tabIndex: 0)
            }
            
            
            tabBar.btnWorkoutForAthlete.removeControlEvent(.touchUpInside)
            tabBar.btnWorkoutForAthlete.addControlEvent(.touchUpInside) {
                
            self.sharedInstance.tabBarController.selectedIndex = 1
            tabBar.updateTabBarForAthelete(tabIndex: 1)
                
            }
            
            tabBar.btnCompareForAthlete.removeControlEvent(.touchUpInside)
            tabBar.btnCompareForAthlete.addControlEvent(.touchUpInside) {
                self.sharedInstance.tabBarController.selectedIndex = 2
                tabBar.updateTabBarForAthelete(tabIndex: 2)
            }
            tabBar.btnProfileForAthlete.removeControlEvent(.touchUpInside)
            tabBar.btnProfileForAthlete.addControlEvent(.touchUpInside) {
                
            self.sharedInstance.tabBarController.selectedIndex = 3
            tabBar.updateTabBarForAthelete(tabIndex: 3)
                
            }
            
           /* tabBar.buttonServiceForRestaturant.removeControlEvent(.touchUpInside)
            tabBar.buttonServiceForRestaturant.addControlEvent(.touchUpInside) {
               self.sharedInstance.tabBarController.selectedIndex = 1
               
                tabBar.updateTabBarForRestaurant(tabIndex: 1)
            }
            
            tabBar.buttonProfileForRestaturant.removeControlEvent(.touchUpInside)
            tabBar.buttonProfileForRestaturant.addControlEvent(.touchUpInside) {
                self.sharedInstance.tabBarController.selectedIndex = 2
                tabBar.updateTabBarForRestaurant(tabIndex: 2)
            }
            
            tabBar.buttonSettingForRestaturant.removeControlEvent(.touchUpInside)
            tabBar.buttonSettingForRestaturant.addControlEvent(.touchUpInside) {
                self.sharedInstance.tabBarController.selectedIndex = 3
                
                tabBar.updateTabBarForRestaurant(tabIndex: 3)
            }*/
        } else {
            
           /* tabBar.buttonBooking.removeControlEvent(.touchUpInside)
            tabBar.buttonBooking.addControlEvent(.touchUpInside) {
                self.sharedInstance.tabBarController.selectedIndex = 0
                tabBar.updateTabBarForService(tabIndex: 0)
            }
            
            tabBar.buttonService.removeControlEvent(.touchUpInside)
            tabBar.buttonService.addControlEvent(.touchUpInside) {
               self.sharedInstance.tabBarController.selectedIndex = 1
                tabBar.updateTabBarForService(tabIndex: 1)
            }
            
            tabBar.buttonEarnings.removeControlEvent(.touchUpInside)
            tabBar.buttonEarnings.addControlEvent(.touchUpInside) {
              self.sharedInstance.tabBarController.selectedIndex = 2
               
                tabBar.updateTabBarForService(tabIndex: 2)
            }
            
            
            tabBar.buttonProfile.removeControlEvent(.touchUpInside)
            tabBar.buttonProfile.addControlEvent(.touchUpInside) {
                self.sharedInstance.tabBarController.selectedIndex = 3
                tabBar.updateTabBarForService(tabIndex: 3)
            }
            
            tabBar.buttonSetting.removeControlEvent(.touchUpInside)
            tabBar.buttonSetting.addControlEvent(.touchUpInside) {
            self.sharedInstance.tabBarController.selectedIndex = 4
             
                tabBar.updateTabBarForService(tabIndex: 4)
            }*/
        }
        self.sharedInstance.tabBarVcObj = tabBar;*/
    }
 
    
  
    
    // MARK:- Change Date Format
    class func convertDateString(dateString : String!, fromFormat sourceFormat : String!, toFormat desFormat : String!) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        let date = dateFormatter.date(from: dateString)
        
        dateFormatter.dateFormat = desFormat
        
        return dateFormatter.string(from: date!)
    }
    
    
    
    //MARK:- Image save, get and delete
    
    public func saveImageToDocumentDirectory(image: UIImage, imageName: String, compressionQuality: CGFloat){
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        let imageData = image.jpegData(compressionQuality: 1)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        
    }
    
    public func getImageFromDocumentDirectory(imageName: String)-> UIImage?{
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: paths){
            return UIImage(contentsOfFile: paths)!
        } else {
            return nil
        }
    }
    
    public func deleteImageFromDocumentDirectory(filename: String){
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(filename)
        if fileManager.fileExists(atPath: paths){
            try! fileManager.removeItem(atPath: paths)
            print("Deleted Succesfully!")
        } else {
            print("File not Exist for Deletion!")
        }
    }
    
    func deleteAllFilesFromDocumentDirectory(){
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        guard let items = try? FileManager.default.contentsOfDirectory(atPath: path) else { return }
        
        for item in items {
            // This can be made better by using pathComponent
            let completePath = path.appending("/").appending(item)
            try? FileManager.default.removeItem(atPath: completePath)
        }
    }
    
    public func getDirectoryPathWithFileName(filename: String) -> String? {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(filename)
        if fileManager.fileExists(atPath: paths){
            return paths
        } else {
            return nil
        }
    }
    
    //MARK:- Image Picker
    
    class func openImagePickerOnViewController(vc: UIViewController , completionhandler:@escaping ImageCompletionBlock, deletionhandler: ImageDeletionBlock? = nil){
        
        self.sharedInstance.completionBlock = completionhandler
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takeAction = UIAlertAction(title: "Take Photo", style: .default) { (alertAction) in
            self.sharedInstance.openCamera(vc: vc)
        }
        alertController.addAction(takeAction)
        
        let chooseAction = UIAlertAction(title: "Choose Photo", style: .default) { (alertAction) in
            self.sharedInstance.openPhotoLibrary(vc: vc)
        }
        alertController.addAction(chooseAction)
        
        if let dHandler = deletionhandler  {
            let deleteHandle = UIAlertAction(title: "Remove Photo", style: .default) { (alertAction) in
                dHandler(true);
            }
            alertController.addAction(deleteHandle)
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    func openCamera(vc: UIViewController) {
        if self.checkCameraAuthorization(vc: vc){
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let picker = UIImagePickerController()
                picker.allowsEditing = false
                picker.delegate = (self as UIImagePickerControllerDelegate & UINavigationControllerDelegate)
                picker.sourceType = .camera
                picker.mediaTypes = [kUTTypeImage as String]
                vc.present(picker, animated: true, completion: nil)
            }
        }
    }
    
    func openPhotoLibrary(vc: UIViewController) {
        if self.checkPhotoLibraryAuthorization(vc: vc){
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let picker = UIImagePickerController()
                picker.allowsEditing = false
                picker.delegate = (self as UIImagePickerControllerDelegate & UINavigationControllerDelegate)
                picker.sourceType = .photoLibrary
                vc.present(picker, animated: true, completion: nil)
            }
        }
    }
    
    public func checkCameraAuthorization(vc: UIViewController) -> Bool {
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            return true
        }else if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.notDetermined{
            return true
        }
        else {
            Common.showAlertForCameraAndPhotoAuthorization(vc: vc,type: "Camera")
            return false
        }
    }
    
    public func checkPhotoLibraryAuthorization(vc: UIViewController) -> Bool {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            return true
        case .denied, .restricted :
            print("denied")
            Common.showAlertForCameraAndPhotoAuthorization(vc: vc,type: "Photo library")
            return false
        case .notDetermined:
            return true
        case .limited:
            return false
        @unknown default:
            return false
        }
    }
    
    class public func showAlertForCameraAndPhotoAuthorization(vc: UIViewController,type: String) {
        
        let appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
        
        let alertController = UIAlertController(title: "Allow " + appName + " access to your "+type,
                                                message: "The "+type+" permission was not authorized. Please enable it in Settings to continue.",
                                                preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
            if let appSettings = NSURL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(appSettings as URL, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    public func showAlert(title:String, msg:String, completionhandler:@escaping (Any?, Int?) -> ()) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (alertAction) in
            completionhandler(nil, 0)
        }
        
        alert.addAction(okAction)
        
        UIApplication.shared.windows.first { $0.isKeyWindow }?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    /*public func showAlert(title:String, msg:String) {
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }*/
    
    // MARK: - Location Checking
    class func isLocationEnabled (targetView:UIView , targetedVC:UIViewController , message:String , actionEnabled:Bool , btnMessage: String)
    {
        var view_toast = CustomToastView()
        if CLLocationManager.locationServicesEnabled() {
            let locationManager = CLLocationManager()
            if #available(iOS 14.0, *) {
                switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied:
                    print("No access")
                    if let viewWithTag = targetView.viewWithTag(1029) {
                        //                    print("Tag 100")
                        viewWithTag.removeFromSuperview()
                    } else {
                        //                    print("tag not found")
                    }
                    view_toast = Common.SetUpToastView(targetView: targetView, targetedVC: targetedVC, message: message, actionEnabled: actionEnabled, buttonText: btnMessage)
                    view_toast.tag = 1029
                    if actionEnabled
                    {
                        view_toast.btn_toastMessage.setTitle(btnMessage, for: .normal)
                    }
                    else
                    {
                        view_toast.btn_toastMessage.setTitle("", for: .normal)
                    }
                case .authorizedAlways, .authorizedWhenInUse:
                    print("Access")
                    if let viewWithTag = targetView.viewWithTag(1029) {
                        //                    print("Tag 100")
                        viewWithTag.removeFromSuperview()
                    } else {
                        //                    print("tag not found")
                    }
                @unknown default:
                    break
                }
            } else {
                // Fallback on earlier versions
            }
        } else {
            print("Location services are not enabled")
            view_toast = Common.SetUpToastView(targetView: targetView, targetedVC: targetedVC, message: message, actionEnabled: actionEnabled, buttonText: btnMessage)
            view_toast.tag = 1029
            if actionEnabled
            {
                view_toast.btn_toastMessage.setTitle(btnMessage, for: .normal)
            }
            else
            {
                view_toast.btn_toastMessage.setTitle("", for: .normal)
            }
            //            print("Location Check Git")
        }
    }
    
    // MARK: - Network Checking
    public func isNetworkEnabled(targetView:UIView , targetedVC:UIViewController , message:String , networkEnabled:Bool , btnMessage: String){
        var toastView = CustomToastView()
        
        if networkEnabled {
            if let viewWithTag = targetView.viewWithTag(1030){
                viewWithTag.removeFromSuperview()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    toastView = Common.SetUpToastView(targetView: targetView, targetedVC: targetedVC, message: message, actionEnabled: true, buttonText: btnMessage)
                    toastView.tag = 1030
                    toastView.lbl_toastMessage.text = message
                    toastView.backgroundColor = #colorLiteral(red: 0, green: 0.6308344603, blue: 0.5691562295, alpha: 1)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        if let tagView = targetView.viewWithTag(1030){
                            tagView.removeFromSuperview()
                        }
                    })
                }
            }
        }
        else
        {
            toastView = Common.SetUpToastView(targetView: targetView, targetedVC: targetedVC, message: message, actionEnabled: true, buttonText: btnMessage)
            toastView.tag = 1030
            toastView.lbl_toastMessage.text = message
            toastView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
    }
    
    class func SetUpToastView(targetView:UIView , targetedVC:UIViewController , message:String , actionEnabled:Bool, buttonText:String) ->CustomToastView
    {
        let ObjCustomView = CustomToastView.instanceFromNib()
        ObjCustomView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height-45, width: UIScreen.main.bounds.width, height: 45)
        
        ObjCustomView.lbl_toastMessage.text = message
        ObjCustomView.btn_toastMessage.setTitle(buttonText, for: .normal)
        if actionEnabled {
            ObjCustomView.btn_toastMessage.isUserInteractionEnabled = true
            ObjCustomView.btn_toastMessage.addTarget(self, action: #selector(handleSettings), for: .touchUpInside)
            targetView.addSubview(ObjCustomView)
            ObjCustomView.bringSubviewToFront(targetView)
        }
        else
        {
            ObjCustomView.btn_toastMessage.isUserInteractionEnabled = false
            ObjCustomView.removeFromSuperview()
        }
        //        ObjCustomView.delegate=targetedVC as? CustomToastView
        
        return ObjCustomView
    }
    
    @objc class func handleSettings(){
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
        }
    }
    
}

extension Common: CropViewControllerDelegate {
    // MARK: - CropViewControllerDelegate
    
    func cropViewController(_ cropViewController: CropViewController, didCropImageToRect rect: CGRect, angle: Int){
        
    }
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int){
        
        /* Dismiss CropViewController */
        cropViewController.dismiss(animated: true, completion: nil)
        
        /* Dismiss UIImagePickerController */
        cropViewController.presentingViewController?.dismiss(animated: true, completion: nil)
        
        let tempImage = image.fixOrientation()
        let resizedImage = Common.resizeImage(image: tempImage, width: 250)
        
        let fileName = self.getStringValueFromAPIValue(apiValue: Date().currentTimestamp, defaultValue: "image.png")+".png"
        self.saveImageToDocumentDirectory(image: image, imageName: fileName, compressionQuality: 0.5)
        self.completionBlock!(true,["fileName": fileName, "imagedata": resizedImage])
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool){
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
}


extension Common: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    // MARK: - UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        let tempImage = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage).fixOrientation()
        let resizedImage = Common.resizeImage(image: tempImage, width: 250)
        
        if let image : UIImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if let imgData = image.jpegData(compressionQuality: 0.5) {
                let imageSize: Int = imgData.count
                let imageSizeInMB = (Double(imageSize) / 1024.0 ) / 1024.0
                print("Size of image in MB: %.2f ", imageSizeInMB)
                
                if imageSizeInMB > 2 {
                    picker.dismiss(animated: true, completion: nil)
                    self.showAlert(title: DefaultMessages.error, msg: DefaultMessages.imageSizeLimit) { (msg, tag) in
                    }
                } else {
                    let cropViewController = CropViewController(image: image)
                    cropViewController.delegate = self
                    picker.present(cropViewController, animated: true, completion: nil)
                }
            } else {
                picker.dismiss(animated: true, completion: nil)
            }
        } else {
            picker.dismiss(animated: true, completion: nil)
        }
    }
}



extension DateFormatter {
    func years(_ range: ClosedRange<Int>) -> [String]  {
        setLocalizedDateFormatFromTemplate("yyyy")
        return Array(range.lowerBound...range.upperBound).compactMap{ DateComponents(calendar: .current, year: $0).date }.compactMap{ string(from: $0) }
        
    }
}

extension NSDate {
    
    var day: Int                { return
        
        Calendar.current.dateComponents([.day, .month, .year, .hour], from: self as Date).day! }
    
    func xDays(x:Int) -> NSDate { return
        (Calendar.current.date(byAdding: .day, value: x, to: self as Date) as NSDate?)! }
    
    func last7days() -> [Int]   { return {var result:[Int] = []; for index in 1...7 { result.append(self.xDays(x: -index).day) }; return result}() }
    
    func nearXdays(days:Int) -> [Int] { return days == 0 ? [self.day] : { var result:[Int] = []; for index in 1...abs(days) { result.append(self.xDays(x: (index * (days>=0 ? 1 : -1))).day) }; return result }() }
    
    func getNextXDates(days:Int) -> [Date] {
        let today = Date()
        return (1...abs(days)).map{ Calendar.current.date(byAdding: .day, value: $0, to: today)!
        }
    }
}

extension UITableView {
    func reloadData(completion: @escaping () -> ()) {
        UIView.animate(withDuration: 0.5, animations: { self.reloadData()})
        {_ in completion() }
    }
}


