//
//  UIColor+Additions.swift
//  Toonyport
//
//  Generated on Zeplin. (9/19/2018).
//  Copyright (c) 2018 Unified Infotech Pvt Ltd. All rights reserved.
//

import UIKit

extension UIColor {

    @nonobjc class var darkishGary1: UIColor {
        return UIColor(red: 63.0 / 255.0, green: 71.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var darkishGary2: UIColor {
        return UIColor(red: 43.0 / 255.0, green: 52.0 / 255.0, blue: 66.0 / 255.0, alpha: 1.0)
    }
}
