//
//  AppExtension.swift
//  Lystant
//
//  Created by Anirban on 11/07/18.
//  Copyright © 2018 Unified Infotech Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire

extension Array {
    func chunked(by chunkSize: Int) -> [[Element]] {
        return stride(from: 0, to: self.count, by: chunkSize).map {
            Array(self[$0..<Swift.min($0 + chunkSize, self.count)])
        }
    }
}

extension UserDefaults {
    enum uDefaultsType : String{
        case maxBasePrice = "maxBasePrice"
        case maxCommssion = "maxCommssion"
    }
    
    class func set(value: String, forKey key: uDefaultsType) {
        let defaults = UserDefaults.standard;
        defaults.set(value, forKey: key.rawValue);
        defaults.synchronize();
    }
    
    class func value(forKey key: uDefaultsType) -> Any? {
        let defaults = UserDefaults.standard;
        return defaults.value(forKey: key.rawValue);
    }

    static func isFirstLaunch() -> Bool {
        let hasBeenLaunchedBeforeFlag = "hasBeenLaunchedBeforeFlag"
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
        if (isFirstLaunch) {
            UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
            UserDefaults.standard.synchronize()
        }
        return isFirstLaunch
    }
}

extension Array where Element: Equatable {
    mutating func removeDuplicates() {
        var result = [Element]()
        for value in self {
            if !result.contains(value) {
                result.append(value)
            }
        }
        self = result
    }
}



// MARK:- NSObject Extension -
extension NSObject {
    class var string: String {
        return String(describing: self);
    }
}

//MARK: - String Extension
extension String
{

    func getClass() -> Swift.AnyClass? {
        return NSClassFromString(self)
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    func isNullString() -> Bool {

        let outputString = self

        let x: AnyObject = NSNull()

        if ((outputString == x as? String) || (outputString.count == 0) || (outputString == " ") || (outputString == "") || (outputString == "(NULL)") || (outputString == "<NULL>") || (outputString == "<null>") || (outputString == "(null)") || (outputString == "nil")) {
            return true
        } else {
            return false
        }
    }
    func isValidEmail() -> Bool {

        let fullNameArr = self.components(separatedBy: "@")
        let firstPart: String = fullNameArr[0]

        if firstPart.rangeOfCharacter(from: NSCharacterSet.letters) != nil {
        } else {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        return result
    }

    func index(at position: Int, from start: Index? = nil) -> Index? {
        let startingIndex = start ?? startIndex
        return index(startingIndex, offsetBy: position, limitedBy: endIndex)
    }

    func character(at position: Int) -> Character? {
        guard position >= 0, let indexPosition = index(at: position) else {
            return nil
        }
        return self[indexPosition]
    }
    
    func isValidNameText() -> Bool {
        let nameRegEx = "[A-Z0-9a-z ]+"
        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        let result = nameTest.evaluate(with: self)
        return result
    }
    func isValidPhoneText() -> Bool {
        let phoneRegEx = "^\\d{0,10}$"
        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        let result = phoneTest.evaluate(with: self)
        return result
    }

    func isValidPassword() -> Bool {

        if(self.count < 2) {
            return false
        } else {
            return true
        }
    }
    
    func isValidPhone() -> Bool {
        if(self.count < 10) {
            return false
        } else {
            return true
        }
    }    

    func isValidZipCode() -> Bool {

        if(self.count != 6) {
            return false
        } else {
            return true
        }
    }

    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    


    //Validate Email
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
        } catch {
            return false
        }
    }

    //Validate Phone Number
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
    subscript(_ range: CountableRange<Int>) -> String {
           let start = index(startIndex, offsetBy: max(0, range.lowerBound))
           let end = index(start, offsetBy: min(self.count - range.lowerBound,
                                                range.upperBound - range.lowerBound))
           return String(self[start..<end])
    }

    subscript(_ range: CountablePartialRangeFrom<Int>) -> String {
           let start = index(startIndex, offsetBy: max(0, range.lowerBound))
            return String(self[start...])
    }
    
    func base64Encoded() -> String? {
            return data(using: .utf8)?.base64EncodedString()
        }

    func base64Decoded() -> String? {
            guard let data = Data(base64Encoded: self) else { return nil }
            return String(data: data, encoding: .utf8)
    }
    
    var charactersArray: [Character] {
        return Array(self)
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
            let fontAttributes = [NSAttributedString.Key.font: font]
            let size = self.size(withAttributes: fontAttributes)
            return size.width
    }
    
    func height(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()

        let height = label.systemLayoutSizeFitting(CGSize(width: width, height: UIView.layoutFittingCompressedSize.height), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel).height
        
        return height
     }
    
    var withoutHtmlTags: String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options:
        .regularExpression, range: nil).replacingOccurrences(of: "&[^;]+;", with:
        "", options:.regularExpression, range: nil)
        }
}

extension Float {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1)  == 0 ? String(format: "%.0f", self) : String(self)
    }
}

// MARK:- Int Extension -
extension Int {
    var success: Bool {
        return self == 1
    }
    func faliure() -> Bool {
        return self == 0
    }
    func expire() -> Bool {
        return self == 404
    }
}

extension CGRect {
    func randomPointInRect() -> CGPoint {
        let origin = self.origin
        return CGPoint(x: CGFloat(arc4random_uniform(UInt32(self.width))) + origin.x, y: CGFloat(arc4random_uniform(UInt32(self.height))) + origin.y)
    }
}

extension UIFont {
    enum kFontType {
        case regular
        case medium
    }
    convenience init?(_ size : CGFloat, _ type: kFontType) {
        switch type {
        case .regular:
            let fontName = "Poppins-Regular"
            self.init(name: fontName, size: size);
        case .medium:
            let fontName = "Poppins-Medium"
            self.init(name: fontName, size: size);
        }
    }
    
}

//MARK: - Date Extension
extension Date {
    var currentTimestamp: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }

    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }

    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date)) years"   }
        if months(from: date)  > 0 { return "\(months(from: date)) months"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date)) weeks"   }
        if days(from: date)    > 0 { return "\(days(from: date)) days"    }
        if hours(from: date)   > 0 { return "\(hours(from: date)) hours"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date)) minutes" }
        if seconds(from: date) > 0 { return "\(seconds(from: date)) seconds" }
        return ""
    }
    
}

//MARK: - UIImage Extension
extension UIImage {
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        } else {
            return self
        }
    }

}




//MARK: - UIColor Extension
extension UIColor {

    /// Get the UIColor from the Hex String
    ///
    /// - Parameter hex: the object which stores the String as hex format to get converted into the UIColor
    convenience init(hex: String) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        
        self.init(red: CGFloat((int & 0xFF0000) >> 16) / 255.0,
                      green: CGFloat((int & 0x00FF00) >> 8) / 255.0,
                      blue: CGFloat(int & 0x0000FF) / 255.0,
                      alpha: 1.0)
    }

    @nonobjc class var waterBlue: UIColor {
        return UIColor(red: 18.0 / 255.0, green: 153.0 / 255.0, blue: 223.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var lightE4Gray: UIColor {
        return UIColor(white: 228.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var pinkishGrey: UIColor {
        return UIColor(white: 202.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var paleGrey: UIColor {
        return UIColor(red: 222.0 / 255.0, green: 228.0 / 255.0, blue: 232.0 / 255.0, alpha: 1.0)
    }

    func getColorDifference(fromColor: UIColor) -> Int {
        // get the current color's red, green, blue and alpha values
        var red:CGFloat = 0
        var green:CGFloat = 0
        var blue:CGFloat = 0
        var alpha:CGFloat = 0
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)

        // get the fromColor's red, green, blue and alpha values
        var fromRed:CGFloat = 0
        var fromGreen:CGFloat = 0
        var fromBlue:CGFloat = 0
        var fromAlpha:CGFloat = 0
        fromColor.getRed(&fromRed, green: &fromGreen, blue: &fromBlue, alpha: &fromAlpha)

        let redValue = (max(red, fromRed) - min(red, fromRed)) * 255
        let greenValue = (max(green, fromGreen) - min(green, fromGreen)) * 255
        let blueValue = (max(blue, fromBlue) - min(blue, fromBlue)) * 255

        return Int(redValue + greenValue + blueValue)
    }

}
//MARK: - UIStoryboard Extension
extension UIStoryboard {

    public class func storyboardMain() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    public class func storyboardHome() -> UIStoryboard {
        return UIStoryboard(name: "Home", bundle: Bundle.main)
    }

}

//MARK: - UIViewController Extension
extension UIViewController {

    func showDefaultAlert(title:String, msg:String) {

        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }

    func showAlertWithAction(title:String, msg:String, completionhandler:@escaping (Any?, Int?) -> ()) {

        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "OK", style: .default) { (alertAction) in
            completionhandler(nil, 0)
        }
        alert.addAction(okAction)

        self.present(alert, animated: true, completion: nil)
    }

    func showAlertWithButtonTitle(title:String, msg:String, buttonTitle:String, completionhandler:@escaping (Any?, Int?) -> ()) {

        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alertAction) in
            completionhandler(nil, 0)
        }
        alert.addAction(cancelAction)

        let otherAction = UIAlertAction(title: buttonTitle, style: .default) { (alertAction) in
            completionhandler(nil, 1)
        }
        alert.addAction(otherAction)

        self.present(alert, animated: true, completion: nil)

    }
    
    func showAlertWithTextField(title:String, msg:String, buttonTitle:String, completionhandler:@escaping (Any?, Int?) -> ()) {
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { (alertAction) -> Void in
            let textField = alert.textFields![0] as UITextField
            
            completionhandler(textField.text, 1)
        }))
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alertAction) in
            completionhandler(nil, 0)
        }
        alert.addAction(cancelAction)
        
        alert.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Email"
        })
        self.present(alert, animated: true, completion: nil)
    }

    func handleError(error : Error) {
        if(error._code == -1009) {
            self.showDefaultAlert(title: DefaultMessages.error, msg: DefaultMessages.kNoNetworkAccess)
        } else if error._code == NSURLErrorTimedOut {
            self.showDefaultAlert(title: DefaultMessages.error, msg: DefaultMessages.timeOut)
        } else {
            self.showDefaultAlert(title: DefaultMessages.error, msg: DefaultMessages.somethingWrong)
        }
    }

    /// Get the View Controller from Storyboard with storyboard ID
    ///
    /// - Parameters:
    ///   - storyboardID: Storyboard ID, the view controller hold in respective storyboard
    ///   - storyboard: storyboard name
    /// - Returns: Required View controller

    public class func getViewController(ofStoryboardId storyboardID: String?, fromStorybord storyboard: String? ) -> UIViewController {

        guard let strStoryboard = storyboard else {

            /// If no viewcontroller name or storyboerd name were passed then get the initialViewController of Main.storyboard, which ever it is
            guard let strStoryboardId = storyboardID else { return UIStoryboard.storyboardMain().instantiateInitialViewController()!;
            }

            ///if no storyboard name passed but only the Viewcontroller then initialize the view controller from main storyboard
            return UIStoryboard.storyboardMain().instantiateViewController(withIdentifier: strStoryboardId);
        }

        /// If storyboard name passed but no view controller then initialize with initial view controller of that storyboard
        guard let strStoryboardId = storyboardID else { return UIStoryboard.init(name: strStoryboard, bundle: Bundle.main).instantiateInitialViewController()! }

        ///else return with requred Viewcontroller or storybaord
        return UIStoryboard.init(name: strStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: strStoryboardId);
    }
    // Screen width.
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }

    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }

    public var safeAreaTopInsect : CGFloat {
        var safeAreaTopInsect : CGFloat = 20.0
        if #available(iOS 11.0, *) {
            if Device.IS_IPHONE_X {
                safeAreaTopInsect =  (UIApplication.shared.windows.first { $0.isKeyWindow }?.safeAreaInsets.top)!
            }
        }
        return safeAreaTopInsect
    }
    
}

//MARK: - UINavigationController Extension
extension UINavigationController {

    public func pushViewController(viewController: UIViewController,
                                         animated: Bool,
                                       completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }

    public func popViewController(animated: Bool,
                                completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)

        let transition = CATransition();
        transition.duration = 0.3;
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut);
        transition.type = CATransitionType.push;
        transition.subtype = CATransitionSubtype.fromRight;
        view.layer.add(transition, forKey: nil);
        popViewController(animated: animated)

        CATransaction.commit()
    }
}

extension UIView {
    
    func borders(for edges:[UIRectEdge], width:CGFloat = 1, color: UIColor = .black) {

            if edges.contains(.all) {
                layer.borderWidth = width
                layer.borderColor = color.cgColor
            } else {
                let allSpecificBorders:[UIRectEdge] = [.top, .bottom, .left, .right]

                for edge in allSpecificBorders {
                    if let v = viewWithTag(Int(edge.rawValue)) {
                        v.removeFromSuperview()
                    }

                    if edges.contains(edge) {
                        let v = UIView()
                        v.tag = Int(edge.rawValue)
                        v.backgroundColor = color
                        v.translatesAutoresizingMaskIntoConstraints = false
                        addSubview(v)

                        var horizontalVisualFormat = "H:"
                        var verticalVisualFormat = "V:"

                        switch edge {
                        case UIRectEdge.bottom:
                            horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                            verticalVisualFormat += "[v(\(width))]-(0)-|"
                        case UIRectEdge.top:
                            horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                            verticalVisualFormat += "|-(0)-[v(\(width))]"
                        case UIRectEdge.left:
                            horizontalVisualFormat += "|-(0)-[v(\(width))]"
                            verticalVisualFormat += "|-(0)-[v]-(0)-|"
                        case UIRectEdge.right:
                            horizontalVisualFormat += "[v(\(width))]-(0)-|"
                            verticalVisualFormat += "|-(0)-[v]-(0)-|"
                        default:
                            break
                        }

                        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: horizontalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: verticalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                    }
                }
            }
        }
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func roundedBottom(){
            let maskPath1 = UIBezierPath(roundedRect: bounds,
                                         byRoundingCorners: [.bottomRight , .bottomLeft],
                                         cornerRadii: CGSize(width: 15, height: 15))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = bounds
            maskLayer1.path = maskPath1.cgPath
            layer.mask = maskLayer1
        }
        func roundedTop(){
            let maskPath1 = UIBezierPath(roundedRect: bounds,
                                         byRoundingCorners: [.topRight , .topLeft],
                                         cornerRadii: CGSize(width: 15, height: 15))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = bounds
            maskLayer1.path = maskPath1.cgPath
            layer.mask = maskLayer1
        }
        func roundedLeft(){
            let maskPath1 = UIBezierPath(roundedRect: bounds,
                                         byRoundingCorners: [.topLeft , .bottomLeft],
                                         cornerRadii: CGSize(width: 15, height: 15))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = bounds
            maskLayer1.path = maskPath1.cgPath
            layer.mask = maskLayer1
        }
        func roundedRight(){
            let maskPath1 = UIBezierPath(roundedRect: bounds,
                                         byRoundingCorners: [.topRight , .bottomRight],
                                         cornerRadii: CGSize(width: 15, height: 15))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = bounds
            maskLayer1.path = maskPath1.cgPath
            layer.mask = maskLayer1
        }
    
    func roundedAllCorner(){
            let maskPath1 = UIBezierPath(roundedRect: bounds,
                                         byRoundingCorners: [.topRight , .bottomRight , .topLeft , .bottomLeft],
                                         cornerRadii: CGSize(width: 15, height: 15))
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame = bounds
            maskLayer1.path = maskPath1.cgPath
            layer.mask = maskLayer1
        }
    
    @IBDesignable
    class GradientView: UIView {
        
        @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
        @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
        @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
        @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
        @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
        @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
        
        override public class var layerClass: AnyClass { return CAGradientLayer.self }
        
        var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
        
        func updatePoints() {
            if horizontalMode {
                gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
                gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
            } else {
                gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
                gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
            }
        }
        func updateLocations() {
            gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
        }
        func updateColors() {
            gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
        }
        
        override public func layoutSubviews() {
            super.layoutSubviews()
            updatePoints()
            updateLocations()
            updateColors()
        }
    }
}

extension NSMutableAttributedString {

    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }

}

extension UITapGestureRecognizer {

 func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
     // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
     let layoutManager = NSLayoutManager()
     let textContainer = NSTextContainer(size: CGSize.zero)
     let textStorage = NSTextStorage(attributedString: label.attributedText!)

     // Configure layoutManager and textStorage
     layoutManager.addTextContainer(textContainer)
     textStorage.addLayoutManager(layoutManager)

     // Configure textContainer
     textContainer.lineFragmentPadding = 0.0
     textContainer.lineBreakMode = label.lineBreakMode
     textContainer.maximumNumberOfLines = label.numberOfLines
     let labelSize = label.bounds.size
     textContainer.size = labelSize

     // Find the tapped character location and compare it to the specified range
     let locationOfTouchInLabel = self.location(in: label)
     let textBoundingBox = layoutManager.usedRect(for: textContainer)
     //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                           //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
     let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

     //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                     // locationOfTouchInLabel.y - textContainerOffset.y);
     let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
     let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
     return NSLocationInRange(indexOfCharacter, targetRange)
 }

}



