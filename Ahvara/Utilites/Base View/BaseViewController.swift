//
//  BaseViewController.swift

//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.
//

import UIKit
import ActionKit
import PureLayout
import SideMenu

class BaseViewController: UIViewController,PSideMenuDelegate {

    var navController = NavBarInitialView()
    var navControllerHome  = NavBarHomeView()
    
    var isNavigationBarVisible : Bool?
    
    var isNavigationHomeBarVisible: Bool?
        
    var menu : SideMenuNavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        self.setBar()
        self.setHomeBar()
    }
    
    func setBar() {
        self.isNavigationBarVisible = true
        self.isNavigationHomeBarVisible = false
        self.addNavigationBar()
    }
    
    func setHomeBar() {
        
        let storyboard = UIStoryboard.storyboardHome()
        let controller = storyboard.instantiateViewController(withIdentifier: "PSideMenuViewController") as! PSideMenuViewController
        controller.delegate = self
        
        menu = SideMenuNavigationController(rootViewController: controller)
        menu?.leftSide = true
        menu?.setNavigationBarHidden(true, animated: true)
        menu?.menuWidth = (UIScreen.main.bounds.width * 3/4)
        self.isNavigationBarVisible = false
        self.isNavigationHomeBarVisible = true
        self.addNavigationBarHome()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.setupNavigationBar()
    }
    
    private func contentControllers() -> [UIViewController] {
        
        let storyboard = UIStoryboard.storyboardMain()
        let controller = storyboard.instantiateViewController(withIdentifier: "ALoginViewController") as! ALoginViewController
        
        return [controller]
    }
    
        
    // MARK:- Add Navigation Bar Home
    private func addNavigationBarHome() {
        
        self.navControllerHome = NavBarHomeView.instanceFromNib()
        navControllerHome.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 80)
        self.view.addSubview(self.navControllerHome);
        
        navControllerHome.autoPinEdge(toSuperviewEdge: .leading, withInset: 0.0)
        navControllerHome.autoPinEdge(toSuperviewEdge: .top, withInset: 0.0)
        navControllerHome.autoPinEdge(toSuperviewEdge: .trailing, withInset: 0.0)
        navControllerHome.autoSetDimension(.height, toSize: 64 + self.safeAreaTopInsect)
        
        navControllerHome.buttonSideMenu.removeControlEvent(.touchUpInside)
        navControllerHome.buttonSideMenu.addControlEvent(.touchUpInside) {
            self.actionButtonLeftSideMenu()
        }
        
        
        navControllerHome.buttonRight1.removeControlEvent(.touchUpInside)
        navControllerHome.buttonRight1.addControlEvent(.touchUpInside) {
            self.actionButtonRight1()
        }
        
        navControllerHome.buttonRight2.removeControlEvent(.touchUpInside)
        navControllerHome.buttonRight2.addControlEvent(.touchUpInside) {
            self.actionButtonRight1()
        }
        
        navControllerHome.buttonRight3.removeControlEvent(.touchUpInside)
        navControllerHome.buttonRight3.addControlEvent(.touchUpInside) {
            self.actionButtonRight1()
        }
    }

    
    // MARK:- Add Navigation Bar
    
    private func addNavigationBar() {
        
        self.navController = NavBarInitialView.instanceFromNib()
        navController.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 80)
        self.view.addSubview(self.navController);
        
        navController.autoPinEdge(toSuperviewEdge: .leading, withInset: 0.0)
        navController.autoPinEdge(toSuperviewEdge: .top, withInset: 0.0)
        navController.autoPinEdge(toSuperviewEdge: .trailing, withInset: 0.0)
        navController.autoSetDimension(.height, toSize: 64 + self.safeAreaTopInsect)
    
        
        navController.buttonLeft1.removeControlEvent(.touchUpInside)
        navController.buttonLeft1.addControlEvent(.touchUpInside) {
            self.actionButtonLeft1()
        }
    
        navController.buttonRight3.removeControlEvent(.touchUpInside)
        navController.buttonRight3.addControlEvent(.touchUpInside) {
            self.actionButtonRight3()
        }
       
    }
    
    func didSelectMenuItem() {
        menu?.dismiss(animated: true, completion: nil)
    }
    
    open func setupNavigationBar() {
        //inherite in child
    }
    
    // MARK:- Nav Bar Button Actions
    func actionButtonLeft1() {
        //showSideMenu()
        //present(sideMenu!, animated: true, completion: nil)
    }
    
    func actionButtonLeftSideMenu() {
        present(menu!, animated: true, completion: nil)
    }
    
    func actionButtonRight1() {
        
    }
    
    func actionButtonRight2() {
        
    }

    func actionButtonRight3() {

    }
    
    func actionButtonAddress() {
        
    }
    
    func hideInitialBar() {
        navController.isHidden = true
        isNavigationBarVisible = false
    }
    
    func hideHomeBar() {
        navControllerHome.isHidden = true
        isNavigationHomeBarVisible = false
    }
    
    func showInitialBar() {
        navController.isHidden = false
        navControllerHome.isHidden = true
        isNavigationBarVisible = true
        isNavigationHomeBarVisible = false
    }
    
    func showHomeBar() {
        navControllerHome.isHidden = false
        navController.isHidden = true
        isNavigationHomeBarVisible = true
        isNavigationBarVisible = false
    }
      // MARK:- status bar Parallax Show
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
     
    
    // MARK:- Show / Hide Nav Bar Buttons
    func hideButtonLeft1() {
        if (isNavigationBarVisible!) {
            navController.buttonLeft1.isHidden = true
        } else {
            navControllerHome.buttonSideMenu.isHidden = true
        }
    }
    
    func showButtonLeft1() {
        if (isNavigationBarVisible!) {
            navController.buttonLeft1.isHidden = false
        } else {
            navControllerHome.buttonSideMenu.isHidden = false
        }
    }
    
    func hideButtonRight1() {
        if !(isNavigationBarVisible!) {
            navControllerHome.buttonRight1.isHidden = true
        }
    }
    
    func showButtonRight1() {
        if !(isNavigationBarVisible!) {
            navControllerHome.buttonRight1.isHidden = false
        }
    }
    
    func hideButtonRight2() {
        if !(isNavigationBarVisible!) {
            navControllerHome.buttonRight2.isHidden = true
        }
    }
    
    func showButtonRight2() {
        if !(isNavigationBarVisible!) {
            navControllerHome.buttonRight2.isHidden = false
        }
    }

    func hideButtonRight3() {
        if (isNavigationBarVisible!) {
            navController.buttonRight3.isHidden = true
        } else {
            navControllerHome.buttonRight3.isHidden = true
        }
    }
    
    func showButtonRight3() {
        if (isNavigationBarVisible!) {
            navController.buttonRight3.isHidden = false
        } else {
            navControllerHome.buttonRight3.isHidden = false
        }
    }
    
    func hideLabelRight1() {
        if (isNavigationBarVisible!) {
            navController.lblRight1.isHidden = true
        }
    }
    
    func showLabelRight1() {
        if (isNavigationBarVisible!) {
            navController.lblRight1.isHidden = false
        }
    }
        
    func hideViewTitle() {
        if(isNavigationBarVisible!) {
            navController.labelTitle.isHidden = true
        } else {
            navControllerHome.labelTitle.isHidden = true
        }
        
    }
    
    func showViewTitle() {
        if(isNavigationBarVisible!) {
            navController.labelTitle.isHidden = false
        } else {
            navControllerHome.labelTitle.isHidden = false
        }
    }
    
    // MARK:- Set Nav Bar title text
    
    func setTitleLabelText(text:String, titleColor: UIColor) {
        
        if(isNavigationBarVisible!) {
            navController.labelTitle.text = text
            navController.labelTitle.textColor = titleColor
        } else {
            navControllerHome.labelTitle.text = text
            navControllerHome.labelTitle.textColor = titleColor
        }
    }
   
    // MARK:- Set Right Label text
    func setLabelRight1Text(text:String, isAttributed: Bool, firstColor: UIColor, secondColor: UIColor) {
        
        if(isNavigationBarVisible!) {
            if(isAttributed) {
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: text)
                
                attributedString.setColorForText(textForAttribute: text[0..<2] , withColor: firstColor)
                attributedString.setColorForText(textForAttribute: text[2...] , withColor: secondColor)
                    navController.lblRight1.attributedText = attributedString
            } else {
                navController.lblRight1.text = text
            }
        }
    }
    
    // MARK:- Change Nav bar Color
    func setBackgroundColor(color:UIColor){
        if(isNavigationBarVisible!) {
            navController.backgroundColor = color
        } else {
            navControllerHome.backgroundColor = color
        }
    }
    
    // MARK:- Set Nav Bar Buttons Image
    func setButtonLeft1Image(image:UIImage) {
        if(isNavigationBarVisible!) {
            navController.buttonLeft1.setImage(image, for: .normal)
        } else {
            navControllerHome.buttonSideMenu.setImage(image, for: .normal)
        }
    }
    
    func setButtonRight1Image(image:UIImage, forControlState: UIControl.State) {
        if !(isNavigationBarVisible!) {
            navControllerHome.buttonRight1.setImage(image, for: forControlState)
        }
    }
    
    func setButtonRight2Image(image:UIImage, forControlState: UIControl.State) {
        if !(isNavigationBarVisible!) {
            navControllerHome.buttonRight2.setImage(image, for: forControlState)
        }
    }
    
    func setButtonRight3Image(image:UIImage, forControlState: UIControl.State) {
        if(isNavigationBarVisible!) {
            navController.buttonRight3.setImage(image, for: forControlState)
        } else {
            navControllerHome.buttonRight3.setImage(image, for: forControlState)
        }
    }

    // MARK:- Set Nav Bar Buttons Title
    func setButtonRight3Title(title: String, titleColor: UIColor) {
        if(isNavigationBarVisible!) {
            navController.buttonRight3.setImage(nil, for: .normal)
            navController.buttonRight3.setTitle(title, for: .normal)
            navController.buttonRight3.setTitleColor(titleColor, for: .normal)
       
        } else {
            navController.buttonRight3.setImage(nil, for: .normal)
            navController.buttonRight3.setTitle(title, for: .normal)
            navController.buttonRight3.setTitleColor(titleColor, for: .normal)
        }
    }
    
    func setButtonRight1Title(title: String, titleColor: UIColor) {
        if !(isNavigationBarVisible!) {
            navControllerHome.buttonRight1.setImage(nil, for: .normal)
            navControllerHome.buttonRight1.setTitle(title, for: .normal)
            navControllerHome.buttonRight1.setTitleColor(titleColor, for: .normal)
        }
    }
    
    func setButtonRight2Title(title: String, titleColor: UIColor) {
        if !(isNavigationBarVisible!) {
            navControllerHome.buttonRight2.setImage(nil, for: .normal)
            navControllerHome.buttonRight2.setTitle(title, for: .normal)
            navControllerHome.buttonRight2.setTitleColor(titleColor, for: .normal)
        }
    }

    func setWidthConstraintButtonRight3(constant: CGFloat) {
        if(isNavigationBarVisible!) {
            navController.constraintWidthButtonRight3.constant = constant
        } else {
            navControllerHome.constraintWidthButtonRight3.constant = constant
        }
        self.view.layoutIfNeeded()
    }
    
    func setWidthConstraintButtonRight1(constant: CGFloat) {
        if(isNavigationBarVisible!) {
            navControllerHome.constraintWidthButtonRight1.constant = constant
        }
        self.view.layoutIfNeeded()
    }
    
    func setWidthConstraintButtonRight2(constant: CGFloat) {
        if(isNavigationBarVisible!)  {
            navControllerHome.constraintWidthButtonRight2.constant = constant
        }
        self.view.layoutIfNeeded()
    }

     // MARK:- Set Button ImageInsets
    func changeButtonRight3ImageInsets(insets: CGFloat) {
        navController.buttonRight3.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: insets)
    }
 }


