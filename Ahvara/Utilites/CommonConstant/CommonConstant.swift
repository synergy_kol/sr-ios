//
//  CommonConstant.swift
//  Panda_Athlete
//
//  Created by Piyali Tarafder on 20/09/21.
//

import Foundation

struct appUIMsg {
    static let kAlertTitel = "Alert"
    static let kAccept = "Do you want to accept this request?"
    static let kDecline = "Do you want to decline this request?"
    static let koK = "Ok"
    static let kLogout = "Do you want to logout?"
    static let kDeleteImage = "Do you want to delete this image?"
    static let kResetPasswordSuccess = "Password Reset Successfully."    
}
