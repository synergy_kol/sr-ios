//
//  NavBarHomeView.swift
//  Panda
//
//  Created by Piyali Tarafder on 22/10/21.
//

import UIKit

class NavBarHomeView: UIView {
    
    @IBOutlet var constraintWidthButtonRight1: NSLayoutConstraint!
    @IBOutlet var constraintWidthButtonRight2: NSLayoutConstraint!
    @IBOutlet var constraintWidthButtonRight3: NSLayoutConstraint!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var buttonSideMenu: UIButton!
    @IBOutlet var buttonRight1: UIButton!
    @IBOutlet var buttonRight2: UIButton!
    @IBOutlet var buttonRight3: UIButton!

    class func instanceFromNib() -> NavBarHomeView {
        
        return UINib(nibName: "NavBarHomeView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NavBarHomeView
    }

}
