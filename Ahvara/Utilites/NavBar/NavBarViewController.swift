//
//  NavBarViewController.swift
//  Toonyport
//
//  Created by Piyali Tarafder on 9/24/18.
//  Copyright © 2018 Unified Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class NavBarViewController: UIViewController {
    
    @IBOutlet var constraintWidthButtonRight3: NSLayoutConstraint!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var buttonLeft1: UIButton!
    @IBOutlet var buttonRight3: UIButton!
    @IBOutlet var viewBackground: DesignableView!
    @IBOutlet var lblRight1: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
