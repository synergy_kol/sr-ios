//
//  NavBarHomeViewController.swift
//  Panda
//
//  Created by Piyali Tarafder on 10/10/21.
//

import UIKit

class NavBarHomeViewController: UIViewController {

    @IBOutlet var constraintWidthButtonRight1: NSLayoutConstraint!
    @IBOutlet var constraintWidthButtonRight2: NSLayoutConstraint!
    @IBOutlet var constraintWidthButtonRight3: NSLayoutConstraint!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var buttonLeft1: UIButton!
    @IBOutlet var buttonRight1: UIButton!
    @IBOutlet var buttonRight2: UIButton!
    @IBOutlet var buttonRight3: UIButton!
    @IBOutlet var viewBackground: DesignableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
