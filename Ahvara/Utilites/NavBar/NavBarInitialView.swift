//
//  NavBarInitialView.swift
//  Panda
//
//  Created by Piyali Tarafder on 22/10/21.
//

import UIKit

class NavBarInitialView: UIView {
    
    @IBOutlet var constraintWidthButtonRight3: NSLayoutConstraint!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var buttonLeft1: UIButton!
    @IBOutlet var buttonRight3: UIButton!
    @IBOutlet var lblRight1: UILabel!
        
        
    class func instanceFromNib() -> NavBarInitialView {
        
        return UINib(nibName: "NavBarInitialView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NavBarInitialView
    }

    
    /*private func setupLayout() {
        NSLayoutConstraint.activate(
            [
                self.topAnchor.constraint(equalTo: topAnchor),
                view.leadingAnchor.constraint(equalTo: leadingAnchor),
                view.bottomAnchor.constraint(equalTo: bottomAnchor),
                view.trailingAnchor.constraint(equalTo: trailingAnchor),
            ]
        )
    }*/
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
